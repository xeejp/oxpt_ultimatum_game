import React, { useReducer, useEffect } from 'react'
import { useStore } from './actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Hidden from '@material-ui/core/Hidden'
import Typography from '@material-ui/core/Typography'

import Instruction from './pages/Instruction'
import Experiment from './pages/Experiment'
import Result from './pages/Result'

import i18nInstance from './i18n'

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(1),
    height: 'calc(100vh -' + theme.spacing(1) + 'px)',
    overflow: 'auto'
  },
  items: {
    padding: theme.spacing(1)
  },
  grid: {
    padding: 0,
    marginBottom: '2.5em'
  }
}))

function renderPage(page) {
  switch (page) {
    case 'instruction':
      return <Instruction />
    case 'experiment':
      return <Experiment />
    case 'result':
      return <Result />
  }
}

export default () => {
  const classes = useStyles()
  const { page, locales } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(page && locales))
    return <></>

  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="flex-start"
        className={classes.grid}
      >
        <Hidden xsDown><Grid item sm={1}></Grid></Hidden>
        <Grid item xs={12} sm={10}>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="stretch"
          >
            <Grid item className={classes.items}>
              <Typography variant='h5'>
                {t('host.title_01')}
              </Typography>
            </Grid>
            <Grid item>
              {renderPage(page)}
            </Grid>
          </Grid>
        </Grid>
        <Hidden xsDown><Grid item sm={1}></Grid></Hidden>
      </Grid>
    </div>
  )
}
