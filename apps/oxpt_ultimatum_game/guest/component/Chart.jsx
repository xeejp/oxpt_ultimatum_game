import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Hidden from '@material-ui/core/Hidden'

import Add from '@material-ui/icons/Add'
import Remove from '@material-ui/icons/Remove'

import IconButton from '@material-ui/core/IconButton'

import i18nInstance from '../i18n'

export default () => {
  const { locales, ultimatumResults, initialAmount } = useStore()
  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  const [chartRound, setchartRound] = useState(0)

  const categories = Array(11).fill(0).map((_, i) => initialAmount * i / 10)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const resultFlatten = () => {
    return Object.keys(ultimatumResults).reduce((accResult, key1) => {
      const result = ultimatumResults[key1]
      return Object.keys(result).reduce((accResult2, key2) => ({
        ...accResult2,
        [key1 + '_' + key2]: result[key2]
      }), accResult)
    }, {})
  }

  const compDataAccept = (round) => {
    const result = (round >= 1) ? ultimatumResults[round] : resultFlatten()
    const values = result ? Object.keys(result).filter(id =>
      result[id] ? result[id].accept : false).map(id => result[id].allo) : []
    return Array.from(categories).map(x => values.filter(y => ((x <= y) && (y < (x + (initialAmount / 10))))).length)
  }

  const compDataRefuse = (round) => {
    const result = (round >= 1) ? ultimatumResults[round] : resultFlatten()
    const values = result ? Object.keys(result).filter(id =>
      result[id] ? !result[id].accept : false).map(id => result[id].allo) : []
    return Array.from(categories).map(x => values.filter(y => ((x <= y) && (y < (x + (initialAmount / 10))))).length)
  }

  const showCategories = (size) => {
    var arr = categories.concat()
    arr = arr.map(function (val, index, array) { return val + (initialAmount / 10 - 1) })
    var newArr = categories.map(function (val, index, array) { return val + '<br>~<br>' + arr[index] })
    newArr[10] = String(initialAmount)
    if (size === 'small') {
      newArr[1] = ''
      newArr[3] = ''
      newArr[5] = ''
      newArr[7] = ''
      newArr[9] = ''
    }
    return newArr
  }

  const handleDec = () => {
    if (chartRound > 0) setchartRound(chartRound - 1)
  }

  const handleInc = () => {
    if (chartRound < Object.keys(ultimatumResults).length) setchartRound(chartRound + 1)
  }

  const variablesObject = () => {
    return typeof (t('variables')) === 'object' ? t('variables') : {}
  }

  const varPointUnit = locales[i18n.language].translations.variables['point_unit_01_s']

  if (!(locales))
    return <></>

  return (
    <>
      <Hidden xsDown>
        <Grid container>
          <Grid item xs={12}>
            <HighchartsReact
              options={{
                chart: {
                  type: 'column'
                },
                title: {
                  text: t('guest.result.result_graph.allo_point_01', variablesObject())
                },
                xAxis: {
                  categories: showCategories('large'),
                  crosshair: true,
                  title: {
                    text: varPointUnit
                  },
                  labels: {
                    step: 1
                  },
                },
                yAxis: {
                  allowDecimals: false,
                  min: 0,
                  title: {
                    text: t('guest.result.result_graph.chart_count_01')
                  },
                  labels: {
                    step: 1
                  }
                },
                tooltip: {
                  headerFormat: `<b>{point.x}${varPointUnit}</b><br/>`,
                  pointFormat: `{series.name}:{point.y}<br/>`,
                  shared: true
                },
                series: [
                  {
                    name: t('guest.result.result_graph.result_accept_01'),
                    data: compDataAccept(chartRound),
                    stacking: 'normal'
                  },
                  {
                    name: t('guest.result.result_graph.result_reject_01'),
                    data: compDataRefuse(chartRound),
                    stacking: 'normal'
                  }
                ],
                credits: {
                  enabled: false
                }
              }}
              highcharts={Highcharts}
            />
          </Grid>
          <Grid item xs={12}>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="baseline"
            >
              <Grid item xs={3}></Grid>
              <Grid item xs={3}>
                <Typography variant='subtitle1'>{(chartRound >= 1) ? t('guest.result.result_graph.chart_round_01', { chart_round: chartRound }) : t('guest.result.result_graph.all_round_01')}</Typography>
              </Grid>
              <Grid item xs={3}>
                <IconButton
                  color="primary"
                  aria-label="decrease round"
                  onClick={handleDec}
                  disabled={chartRound <= 0}
                >
                  <Remove />
                </IconButton>
                <IconButton
                  color="secondary"
                  aria-label="increase round"
                  onClick={handleInc}
                  disabled={chartRound >= Object.keys(ultimatumResults).length}
                >
                  <Add />
                </IconButton>
              </Grid>
              <Grid item xs={3}></Grid>
            </Grid>
          </Grid>
        </Grid>
      </Hidden>
      <Hidden smUp>
        <Grid container>
          <Grid item xs={12}>
            <HighchartsReact
              options={{
                chart: {
                  type: 'column'
                },
                title: {
                  text: t('guest.result.result_graph.allo_point_01', variablesObject())
                },
                xAxis: {
                  categories: showCategories('small'),
                  crosshair: true,
                  title: {
                    text: varPointUnit
                  },
                  labels: {
                    step: 1
                  },
                },
                yAxis: {
                  allowDecimals: false,
                  min: 0,
                  title: {
                    text: t('guest.result.result_graph.chart_count_01')
                  },
                  labels: {
                    step: 1
                  }
                },
                tooltip: {
                  headerFormat: `<b>{point.x}${varPointUnit}</b><br/>`,
                  pointFormat: `{series.name}:{point.y}<br/>`,
                  shared: true
                },
                series: [
                  {
                    name: t('guest.result.result_graph.result_accept_01'),
                    data: compDataAccept(chartRound),
                    stacking: 'normal'
                  },
                  {
                    name: t('guest.result.result_graph.result_reject_01'),
                    data: compDataRefuse(chartRound),
                    stacking: 'normal'
                  }
                ],
                credits: {
                  enabled: false
                }
              }}
              highcharts={Highcharts}
            />
          </Grid>
          <Grid item xs={12}>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="baseline"
            >
              <Grid item xs={3}></Grid>
              <Grid item xs={3}>
                <Typography variant='subtitle1'>{(chartRound >= 1) ? t('guest.result.result_graph.chart_round_01', { chart_round: chartRound }) : t('guest.result.result_graph.all_round_01')}</Typography>
              </Grid>
              <Grid item xs={3}>
                <IconButton
                  color="primary"
                  aria-label="decrease round"
                  onClick={handleDec}
                  disabled={chartRound <= 0}
                >
                  <Remove />
                </IconButton>
                <IconButton
                  color="secondary"
                  aria-label="increase round"
                  onClick={handleInc}
                  disabled={chartRound >= Object.keys(ultimatumResults).length}
                >
                  <Add />
                </IconButton>
              </Grid>
              <Grid item xs={3}></Grid>
            </Grid>
          </Grid>
        </Grid>
      </Hidden>
    </>
  )
}
