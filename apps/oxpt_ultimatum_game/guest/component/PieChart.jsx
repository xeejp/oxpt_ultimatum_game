import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

import i18nInstance from '../i18n'

const PieChart = ({ mode }) => {
  const { locales, groups, playerNum, finishedNum } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const name =
    mode === 'finished'
      ? [t('playing'), t('finished')]
      : [t('read'), t('reading')]

  let plotOptions = {
    dataLabels: {
      enabled: true,
      format: '<b>{point.name}</b>: {point.y}'
    }
  }

  let data = []

  const playerNumber = playerNum
  const finishedNumber = finishedNum
  const readNumber = 0

  if (mode === 'finished') {
    Object.assign(plotOptions, {
      startAngle: -90,
      endAngle: 90,
      center: ['50%', '75%']
    })

    const unfinishedNumber = playerNumber - finishedNumber

    data = [
      {
        name: name[1],
        y: finishedNumber,
        color: '#7cb5ec'
      },
      {
        name: name[0],
        y: unfinishedNumber,
        color: '#f7a35c'
      }
    ]
  } else {
    const unreadNumber = playerNumber - readNumber

    data = [
      {
        name: name[0],
        y: readNumber,
        color: '#7cb5ec'
      },
      {
        name: name[1],
        y: unreadNumber,
        color: '#f7a35c'
      }
    ]
  }

  if (!(locales && playerNumber !== undefined && finishedNumber !== undefined))
    return <></>

  return (
    <HighchartsReact
      options={{
        title: {
          text: ''
        },
        tooltip: {
          pointFormat: '<b>{point.y}</b>'
        },
        plotOptions: {
          pie: plotOptions
        },
        series: [
          {
            type: 'pie',
            innerSize: '50%',
            data: data
          }
        ],
        credits: {
          enabled: false
        }
      }}
      highcharts={Highcharts}
    />
  )
}

PieChart.propTypes = {
  mode: PropTypes.string
}

export default PieChart
