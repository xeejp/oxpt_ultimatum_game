import React, { useEffect } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { useTranslation } from 'react-i18next'

import i18nInstance from './i18n'
import configureStore from './store/configureStore'
import channel from 'oxpt'
import App from './App'

channel.join()
const store = configureStore()

const styles = _theme => ({
})

const UltimatumGame = withStyles(styles)(props => {
  const [, i18n] = useTranslation('translations', { i18nInstance })

  const messageListener = event => {
    if (event.data.lang && event.data.lang !== i18n.language) {
      i18n.changeLanguage(event.data.lang)
    }
  }

  useEffect(() => {
    window.addEventListener('message', messageListener, false)
    return () =>
      window.removeEventListener('message', messageListener, false)
  }, [])

  return (
    <Provider store={store}>
      <App />
    </Provider>
  )
})

ReactDOM.render(<UltimatumGame />, document.getElementById('root'))
