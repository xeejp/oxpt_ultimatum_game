import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import CircularProgress from '@material-ui/core/CircularProgress'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogActions from '@material-ui/core/DialogActions'

import Slider from './Slider'
import AllocatePieChart from '../util/AllocatePieChart'
import { fromCamelToSnake } from '../util'
import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  sliderWrapper: {
    marginTop: '45.25px'
  },
  outer: {
    padding: theme.spacing(1)
  },
  dialogContent: {
    padding: theme.spacing(1),
    align: 'justify'
  },
  button: {
    padding: theme.spacing(2)
  },
  buttonGrid1: {
    paddingRight: theme.spacing(1),
  },
  buttonGrid2: {
    paddingLeft: theme.spacing(1),
  },
  allocatePieChart: {
    maxWidth: '90%'
  }
}))

export default () => {
  const classes = useStyles()
  const { locales, role, group, initialAmount, requestState } = useStore()
  const { alloTemp } = group

  const [open, setOpen] = useState(false)
  const [sliderValue, setSliderValue] = useState(alloTemp)
  const [wait, setWait] = useState(false)

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales && group))
    return <></>

  const handleThinking = (_, value) => {
    setSliderValue(value)
  }

  const handleChanging = (_, value) => {
    setSliderValue(value)
  }

  const handleClick = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleSend = () => {
    setWait(true)
    requestState({ event: 'finish allocating' , payload: sliderValue, successCallback: callbackSend, timeoutCallback: callbackSend })
  }

  const callbackSend = () => {
    setWait(false)
  }

  const variablesObject = () => {
    return (typeof t('variables')) === 'object' ? t('variables') : {}
  }

  const plural = (n, unit) => {
    if (n > 1) {
      return fromCamelToSnake(unit) + '_m'
    } else {
      return fromCamelToSnake(unit) + '_s'
    }
  }

  const showAllocateConfirm = () => {
    if (i18n.language === 'en') {
      return t(plural(sliderValue, 'guest.experiment.allocating.allocate_confirm_user_01'), { ...variablesObject(), user_allo: sliderValue }) + t(plural(initialAmount - sliderValue, 'guest.experiment.allocating.allocate_confirm_enemy_01'), { ...variablesObject(), enemy_allo: initialAmount - sliderValue }) + t(plural(initialAmount, 'guest.experiment.allocating.allocate_confirm_initial_01'), { ...variablesObject(), initial_amount: initialAmount })
    } else {
      return t(plural(initialAmount, 'guest.experiment.allocating.allocate_confirm_initial_01'), { ...variablesObject(), initial_amount: initialAmount }) + t(plural(sliderValue, 'guest.experiment.allocating.allocate_confirm_user_01'), { ...variablesObject(), user_allo: sliderValue }) + t(plural(initialAmount - sliderValue, 'guest.experiment.allocating.allocate_confirm_enemy_01'), { ...variablesObject(), enemy_allo: initialAmount - sliderValue })
    }
  }

  const enemy = (role === 'proposer') ? 'responder' : 'proposer'
  const varRole = locales[i18n.language].translations.variables[role + '_01']
  const varEnemy = locales[i18n.language].translations.variables[enemy + '_01']

  return (
    <Card className={classes.outer}>
      <CardContent>
        <Typography variant='subtitle1'>{t('guest.experiment.allocating.user_role_01', { role: varRole })}</Typography>
        {(role === 'proposer')
          ? <>
            <Typography color='textSecondary'>{t('guest.experiment.allocating.user_allocate_01')}</Typography>
            <Typography variant='body1'>{t(plural(sliderValue, 'guest.experiment.allocating.user_allo_01'), { ...variablesObject(), user_allo: sliderValue })}</Typography>
            <Typography variant='body1'>{t(plural(initialAmount - sliderValue, 'guest.experiment.allocating.enemy_allo_01'), { ...variablesObject(), enemy_allo: initialAmount - sliderValue, enemy: varEnemy })}</Typography>
            <div className={classes.sliderWrapper}>
              <Slider
                min={0}
                max={Math.floor(initialAmount)}
                step={1}
                value={sliderValue}
                onChange={handleChanging}
                onChangeCommitted={handleThinking}
              />
            </div>
            <div className={classes.allocatePieChart}>
              <AllocatePieChart
                userAllo={sliderValue}
                enemyAllo={initialAmount - sliderValue}
                enemy={varEnemy}
              />
            </div>
            <Dialog
              open={open}
              onClose={handleClose}
            >
              <DialogContent className={classes.dialogContent}>
                <DialogContentText>
                  {showAllocateConfirm()}
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Grid
                  container
                  direction="row"
                  justify="flex-end"
                  alignItems="flex-start"
                >
                  <Grid item xs={6} sm={4} className={classes.buttonGrid1}>
                    <Button variant='outlined' onClick={handleClose} color='secondary' className={classes.button} fullWidth>
                      {t('guest.experiment.allocating.cancel_01')}
                    </Button>
                  </Grid>
                  <Grid item xs={6} sm={4} className={classes.buttonGrid2}>
                    <Button variant='contained' onClick={handleSend} color='primary' className={classes.button} fullWidth>
                      {t('guest.experiment.allocating.propose_01')}
                    </Button>
                  </Grid>
                </Grid>
              </DialogActions>
            </Dialog>
          </>
          : <Typography color='textSecondary'>{t('guest.experiment.allocating.enemy_allocate_01', { enemy: varEnemy })}</Typography>}
      </CardContent>
      {(role === 'proposer') && <CardActions>
        <Grid
          container
          direction="row"
          justify="flex-end"
          alignItems="flex-start"
        >
          <Grid item xs={12} sm={4}>
            <Button
              color='primary'
              variant='contained'
              disabled={role === 'responder'}
              onClick={handleClick}
              className={classes.button}
              fullWidth
            >
              {wait && <CircularProgress size={24} className={classes.buttonProgress}/>}
              {!wait && t('guest.experiment.allocating.next_01')}
            </Button>
          </Grid>
        </Grid>
      </CardActions>}
    </Card>
  )
}
