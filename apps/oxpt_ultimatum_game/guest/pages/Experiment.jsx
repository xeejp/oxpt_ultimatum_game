import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Hidden from '@material-ui/core/Hidden'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'

import Paper from '@material-ui/core/Paper'

import i18nInstance from '../i18n'
import Allocating from './Allocating'
import Judging from './Judging'
import Notice from './Notice'
import Finished from './Finished'

import { fromCamelToSnake } from '../util'

const useStyles = makeStyles(theme => ({
  items: {
    padding: theme.spacing(1),
  }
}))

function renderPage(gameState) {
  switch (gameState) {
    case 'allocating':
      return <Allocating />
    case 'judging':
      return <Judging />
    case 'finished':
      return <Finished />
  }
}

export default () => {
  const classes = useStyles()
  const { locales, gameRounds, group, infRedo, gameRedo, point, role, requestState } = useStore()
  if (!(locales && group))
    return <></>
  const { gameState, nowRound, redoCount, response } = group

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  const [alloFlag, setAlloFlag] = useState(false)
  const [responseOK, setResponseOK] = useState(false)
  const [responseNG, setResponseNG] = useState(false)
  const [responseREDO, setResponseREDO] = useState(false)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  useEffect(() => {
    if (gameState === 'judging' && role === 'responder') {
      handleCloseResponse()
      setAlloFlag(true)
    }
    if ((gameState === 'allocating' || gameState === 'finished') && role === 'responder' && response === 'OK') setResponseOK(true)
    if ((gameState === 'allocating' || gameState === 'finished') && role === 'responder' && response === 'NG') setResponseNG(true)
    if (gameState === 'allocating' && role === 'proposer' && response === 'REDO') setResponseREDO(true)
  }, [gameState])

  const handleClose = () => {
    setAlloFlag(false)
  }

  const handleCloseResponse = () => {
    setResponseOK(false)
    setResponseNG(false)
    setResponseREDO(false)
    requestState({ event: 'reset response' })
  }

  const variablesObject = () => {
    return ((typeof t('variables')) === 'object' ? t('variables') : {})
  }

  const plural = (n, unit) => {
    if (n > 1) {
      return fromCamelToSnake(unit) + '_m'
    } else {
      return fromCamelToSnake(unit) + '_s'
    }
  }

  if (role === 'visitor') {
    return (
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="stretch"
        className={classes.outer}
      >
        <Grid item sx={12} sm={10}>
          <Typography variant="body1">{t('guest.experiment.error.cant_join_01')}</Typography>
        </Grid>
      </Grid>
    )
  }

  return (
    <>
      <Grid item className={classes.items}>
        {renderPage(gameState)}
      </Grid>
      <Hidden xsDown>
        <Grid item className={classes.items}>
          <Paper>
            <Table size="small">
              <TableHead>
                <TableRow>
                  <TableCell variant='head' width='25%'>{t('guest.experiment.info_table.header_round_01')}</TableCell>
                  <TableCell variant='head' width='25%'>{t('guest.experiment.info_table.header_role_change_01')}</TableCell>
                  <TableCell variant='head' width='25%'>{t('guest.experiment.info_table.header_suggestion_01')}</TableCell>
                  <TableCell variant='head' width='25%'>{t('guest.experiment.info_table.header_point_01')}</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell variant='body'>{t(plural(nowRound, 'guest.experiment.info_table.round_view_now_01'), { now_round: nowRound })}{t(plural(gameRounds, 'guest.experiment.info_table.round_view_total_01'), { game_rounds: gameRounds })}{(gameRounds - nowRound) === 0 && <><br />{'(' + t('guest.experiment.info_table.final_round_01') + ')'}</>}</TableCell>
                  <TableCell variant='body'>{t(plural(gameRounds - nowRound, 'guest.experiment.info_table.role_change_view_01'), { round: gameRounds - nowRound })}</TableCell>
                  <TableCell variant='body'>
                    {infRedo ? t('guest.experiment.info_table.inf_redo_01') : t(plural(redoCount, 'guest.experiment.info_table.suggestion_view_now_01'), { redo_count: redoCount })}{infRedo ? null : t(plural(gameRedo, 'guest.experiment.info_table.suggestion_view_total_01'), { game_redo: gameRedo })}
                    {(gameRedo - redoCount) === 0 && !infRedo && <><br />{'(' + t('guest.experiment.info_table.final_suggestion_01') + ')'}</>}
                  </TableCell>
                  <TableCell variant='body'>{point}{t(plural(point, 'variables.point_unit_01'))}</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </Paper>
        </Grid>
      </Hidden>
      <Hidden smUp>
        <Grid item className={classes.items}>
          <Paper>
            <Table size="small">
              <TableHead>
                <TableRow>
                  <TableCell variant='head'>{t('guest.experiment.info_table.header_item_01')}</TableCell>
                  <TableCell variant='head'>{t('guest.experiment.info_table.header_info_01')}</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell variant='head'>{t('guest.experiment.info_table.header_round_01')}</TableCell>
                  <TableCell variant='body'>{t(plural(nowRound, 'guest.experiment.info_table.round_view_now_01'), { now_round: nowRound })}{t(plural(gameRounds, 'guest.experiment.info_table.round_view_total_01'), { game_rounds: gameRounds })}{(gameRounds - nowRound) === 0 && <><br />{'(' + t('guest.experiment.info_table.final_round_01') + ')'}</>}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell variant='head'>{t('guest.experiment.info_table.header_role_change_01')}</TableCell>
                  <TableCell variant='body'>{t(plural(gameRounds - nowRound, 'guest.experiment.info_table.role_change_view_01'), { round: gameRounds - nowRound })}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell variant='head'>{t('guest.experiment.info_table.header_suggestion_01')}</TableCell>
                  <TableCell variant='body'>
                    {infRedo ? t('guest.experiment.info_table.inf_redo_01') : t(plural(redoCount, 'guest.experiment.info_table.suggestion_view_now_01'), { redo_count: redoCount })}{infRedo ? null : t(plural(gameRedo, 'guest.experiment.info_table.suggestion_view_total_01'), { game_redo: gameRedo })}
                    {(gameRedo - redoCount) === 0 && !infRedo && <><br />{'(' + t('guest.experiment.info_table.final_suggestion_01') + ')'}</>}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell variant='head'>{t('guest.experiment.info_table.header_point_01')}</TableCell>
                  <TableCell variant='body'>{point}{t(plural(point, 'variables.point_unit_01'))}</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </Paper>
        </Grid>
      </Hidden>
      <Notice
        open={alloFlag}
        handleClose={handleClose}
        message={t('guest.experiment.judging.judging_notice_01', variablesObject())}
      />
      <Notice
        open={responseOK}
        handleClose={handleCloseResponse}
        message={t('guest.experiment.response.response_OK_01', variablesObject())}
      />
      <Notice
        open={responseNG}
        handleClose={handleCloseResponse}
        message={t('guest.experiment.response.response_NG_01', variablesObject())}
      />
      <Notice
        open={responseREDO}
        handleClose={handleCloseResponse}
        message={t('guest.experiment.response.response_REDO_01', variablesObject())}
      />
    </>
  )
}
