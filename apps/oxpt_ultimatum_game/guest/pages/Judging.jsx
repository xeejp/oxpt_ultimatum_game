import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import CircularProgress from '@material-ui/core/CircularProgress'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'

import AllocatePieChart from '../util/AllocatePieChart'

import i18nInstance from '../i18n'
import { fromCamelToSnake } from '../util'

const useStyles = makeStyles(theme => ({
  button: {
    padding: theme.spacing(2)
  },
  buttonGrid1: {
    paddingRight: theme.spacing(1)
  },
  buttonGrid2: {
    paddingLeft: theme.spacing(1)
  },
  allocatePieChart: {
    maxWidth: '90%'
  }
}))

export default () => {
  const classes = useStyles()
  const { locales, role, group, infRedo, gameRedo, initialAmount, requestState } = useStore()
  const { alloTemp, redoCount } = group

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)
  const [wait, setWait] = useState(false)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales && group))
    return <></>

  const handleResponse = (accept) => {
    if (!accept && (infRedo || redoCount < gameRedo)) {
      requestState({ event: 'redo allocating', successCallback: callbackSend, timeoutCallback: callbackSend })
    } else {
      requestState({ event: 'response', payload: accept, successCallback: callbackSend, timeoutCallback: callbackSend })
    }
  }

  const callbackSend = () => {
    setWait(false)
  }

  const variablesObject = () => {
    return (typeof t('variables')) === 'object' ? t('variables') : {}
  }

  const plural = (n, unit) => {
    if (n > 1) {
      return fromCamelToSnake(unit) + '_m'
    } else {
      return fromCamelToSnake(unit) + '_s'
    }
  }

  const enemy = (role === 'proposer') ? 'responder' : 'proposer'
  const varRole = locales[i18n.language].translations.variables[role + '_01']
  const varEnemy = locales[i18n.language].translations.variables[enemy + '_01']

  return (
    <Card>
      <CardContent>
        <Typography variant='subtitle1'>{t('guest.experiment.allocating.user_role_01', { 'role': varRole })}</Typography>
        {(role === 'proposer')
          ? <>
            <Typography variant='body1'>{t(plural(alloTemp, 'guest.experiment.allocating.user_allo_01'), { ...variablesObject(), user_allo: alloTemp })}</Typography>
            <Typography variant='body1'>{t(plural(initialAmount - alloTemp, 'guest.experiment.allocating.enemy_allo_01'), { ...variablesObject(), enemy_allo: initialAmount - alloTemp, 'enemy': varEnemy })}</Typography>
            <Typography variant='body1'>{t('guest.experiment.judging.send_wait_01', { ...variablesObject(), enemy: varEnemy })}</Typography>
          </>
          : <>
            <Typography variant='body1'>{t(plural(initialAmount - alloTemp, 'guest.experiment.allocating.user_allo_01'), { ...variablesObject(), user_allo: initialAmount - alloTemp })}</Typography>
            <Typography variant='body1'>{t(plural(alloTemp, 'guest.experiment.allocating.enemy_allo_01'), { ...variablesObject(), enemy_allo: alloTemp, 'enemy': varEnemy })}</Typography>
            <Typography variant='body1'>{t('guest.experiment.judging.suggestion_01', variablesObject())}</Typography>
            <div className={classes.allocatePieChart}>
              <AllocatePieChart
                userAllo={initialAmount - alloTemp}
                enemyAllo={alloTemp}
                enemy={varEnemy}
              />
            </div>
          </>}
      </CardContent>
      {(role === 'proposer') ? null
        : <CardActions>
          <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="flex-start"
          >
            <Grid item xs={6} sm={4} className={classes.buttonGrid1}>
              <Button
                color='primary'
                variant='contained'
                className={classes.button}
                fullWidth
                onClick={handleResponse.bind(null, true)}
              >
                {wait && <CircularProgress size={24} className={classes.buttonProgress}/>}
                {!wait && t('guest.experiment.judging.accept_01')}
              </Button>
            </Grid>
            <Grid item xs={6} sm={4} className={classes.buttonGrid2}>
              <Button
                color='secondary'
                variant='contained'
                className={classes.button}
                fullWidth
                onClick={handleResponse.bind(null, false)}
              >
                {wait && <CircularProgress size={24} className={classes.buttonProgress}/>}
                {!wait && t('guest.experiment.judging.reject_01')}
              </Button>
            </Grid>
          </Grid>
        </CardActions>}
    </Card>
  )
}
