import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'

import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  button: {
    padding: theme.spacing(2)
  },
}))

const Notice = ({ open, handleClose, message }) => {
  const classes = useStyles()

  const { locales } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales))
    return <></>

  return (
    <Dialog
      open={open}
      onClose={handleClose}
    >
      <DialogContent>
        <DialogContentText>
          {message}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Grid
          container
          direction="row"
          justify="flex-end"
          alignItems="flex-start"
        >
          <Grid item xs={12} sm={4}>
            <Button onClick={handleClose} color="secondary" variant='contained' fullWidth className={classes.button}>
              {t('guest.experiment.response.close_01')}
            </Button>
          </Grid>
        </Grid>
      </DialogActions>

    </Dialog>
  )
}

Notice.propTypes = {
  open: PropTypes.bool,
  handleClose: PropTypes.func,
  message: PropTypes.string
}

export default Notice
