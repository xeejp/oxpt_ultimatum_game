import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import clsx from 'clsx'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import RoundResult from './RoundResult'
import Chart from '../component/Chart'
import i18nInstance from '../i18n'
import grey from '@material-ui/core/colors/grey'

const useStyles = makeStyles(theme => ({
  items: {
    padding: theme.spacing(1)
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    })
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  inner: {
    margin: theme.spacing(1)
  },
  actionHeader: {
    backgroundColor: grey[200]
  }
}))

export default () => {
  const classes = useStyles()
  const { locales } = useStore()

  const [expandGraph, setExpandGraph] = useState(true)
  const [expandResult, setExpandResult] = useState(false)

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const handleExpandGraph = () => {
    setExpandGraph(!expandGraph)
  }

  const handleExpandResult = () => {
    setExpandResult(!expandResult)
  }

  if (!(locales))
    return <></>

  return (
    <>
      <Grid container>
        <Grid item xs={12} className={classes.inner}>
          <Card>
            <CardHeader
              className={classes.actionHeader}
              action={
                <IconButton
                  className={clsx(classes.expand, {
                    [classes.expandOpen]: expandGraph,
                  })}
                  onClick={handleExpandGraph}
                  aria-expanded={expandGraph}
                  aria-label="expandGraph"
                >
                  <ExpandMoreIcon />
                </IconButton>
              }
              title={t('guest.result.result_graph.title_01')}
            />
            <Collapse in={expandGraph}>
              <CardContent>
                <Chart />
              </CardContent>
            </Collapse>
          </Card>
        </Grid>
        <Grid item xs={12} className={classes.inner}>
          <Card>
            <CardHeader
              className={classes.actionHeader}
              action={
                <IconButton
                  className={clsx(classes.expand, {
                    [classes.expandOpen]: expandResult,
                  })}
                  onClick={handleExpandResult}
                  aria-expanded={expandResult}
                  aria-label="expandResult"
                >
                  <ExpandMoreIcon />
                </IconButton>
              }
              title={t('guest.result.round_result.title_01')}
            />
            <Collapse in={expandResult}>
              <CardContent>
                <RoundResult/>
              </CardContent>
            </Collapse>
          </Card>
        </Grid>
      </Grid>
    </>
  )
}
