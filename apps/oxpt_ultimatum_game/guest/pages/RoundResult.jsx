import React, { useReducer, useEffect } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Hidden from '@material-ui/core/Hidden'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  items: {
    padding: theme.spacing(1)
  }
}))

export default () => {
  const classes = useStyles()
  const { locales, group, initialAmount } = useStore()
  const { groupResults } = group

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const varResponder = locales[i18n.language].translations.variables['responder_01']
  const varProposer = locales[i18n.language].translations.variables['proposer_01']

  if (!(locales))
    return <></>

  return (
    <TableContainer>
      <Table aria-label="result table">
        <TableHead>
          <TableRow>
            <TableCell align="center">{t('host.setting.game.num_of_round_01')}</TableCell>
            <TableCell align="center">{t('guest.result.round_result.your_role_01')}</TableCell>
            <TableCell align="center">{t('guest.result.round_result.distribution_ratio_01')}<br />（{ varProposer }：{ varResponder }）</TableCell>
            <TableCell align="center">{t('guest.result.round_result.result_page_01')}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {groupResults.concat().reverse().map(({ proposer, allo, accepted }, index) => (
            <TableRow key={index}>
              <TableCell align="right">{index + 1}</TableCell>
              {(proposer === _oxpt.guest_id) ? <TableCell align="left">{ varProposer }</TableCell> : <TableCell align="left">{ varResponder }</TableCell>}
              <TableCell align="center">{allo} : {(initialAmount - allo)}</TableCell>
              {accepted
                ? <TableCell align="center" className={classes.items}>
                  <Hidden xsDown><Button variant="outlined" disabled>{t('guest.result.result_graph.result_reject_01')}</Button>&nbsp;</Hidden>
                  <Button variant="contained" color="primary" disableElevation={true} disableFocusRipple={true} disableRipple={true}>{t('guest.result.result_graph.result_accept_01')}</Button>
                </TableCell>
                : <TableCell align="center" className={classes.items}>
                  <Button variant="contained" color="secondary" disableElevation={true} disableFocusRipple={true} disableRipple={true}>{t('guest.result.result_graph.result_reject_01')}</Button>
                  <Hidden xsDown>&nbsp;<Button variant="outlined" disabled>{t('guest.result.result_graph.result_accept_01')}</Button></Hidden>
                </TableCell>
              }
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}
