import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'

import { makeStyles } from '@material-ui/core/styles'
import MUISlider from '@material-ui/core/Slider'

const useStyles = makeStyles(theme => ({
  mark: {
    backgroundColor: '#000',
    height: 8,
    width: 1,
    marginTop: -3
  }
}))

const Slider = ({ value, step, onChange, onChangeCommitted, min, max }) => {
  const classes = useStyles()
  const center = Math.floor((max - min) / 2 / step) * step
  const marks = []
  for (let i = min; i <= max; i += step * 100) {
    marks.push({
      value: i,
      label: i + ''
    })
  }

  useEffect(() => {
    const delta = 4
    setAnimation(true)
    setAnimationProgress(center)

    let delay = 60
    for (let i = center; i <= (max - (max * 0.2)); i += step * (max * 0.1)) {
      setTimeout(() => {
        setAnimationProgress(i)
      }, delay)
      delay += delta
    }

    for (let i = (max - (max * 0.2)); i >= min + (max * 0.2); i -= step * (max * 0.1)) {
      setTimeout(() => {
        setAnimationProgress(i)
      }, delay)
      delay += delta
    }

    for (let i = min + (max * 0.2); i <= center; i += step * (max * 0.1)) {
      setTimeout(() => {
        setAnimationProgress(i)
      }, delay)
      delay += delta
    }

    setTimeout(() => {
      setAnimation(false)
    }, delay)
  }, [])

  const onChangeSlider = (event, number) => {
    if (number !== value) {
      onChange(event, number)
    }
  }

  const [animation, setAnimation] = useState(false)
  const [animationProgress, setAnimationProgress] = useState(-1)

  return (
    <MUISlider
      onChange={onChangeSlider}
      onChangeCommitted={onChangeCommitted}
      valueLabelDisplay="on"
      marks={marks}
      classes={{
        mark: classes.mark
      }}
      step={step}
      min={min}
      max={max}
      value={animation ? animationProgress : value}
    />
  )
}

Slider.propTypes = {
  value: PropTypes.number,
  step: PropTypes.number,
  onChange: PropTypes.func,
  onChangeCommitted: PropTypes.func,
  min: PropTypes.number,
  max: PropTypes.number
}

export default Slider
