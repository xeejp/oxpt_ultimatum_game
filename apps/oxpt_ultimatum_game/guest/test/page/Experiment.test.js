/* eslint-env jest */
import React from 'react'
import renderer from 'react-test-renderer'
import sinon from 'sinon'
import * as hooks from '../../actions/hook'

import Experiment from '../../pages/Experiment'

describe('experiment', () => {
  const pushState = jest.fn()

  const default_state = {
    locales: {
      en: {
        translations: {
          title: 'UltimatumGame',
          result: '\nYou are #{{rank}} of {{total}}.\n'
        }
      },
      ja: {
        translations: {
          title: 'ボイラープレート',
          result: '\nあなたは{{total}}人中#{{rank}}位でした。\n'
        }
      }
    },
    game_redo: 0,
    game_rounds: 1,
    group: {
      allo_temp: 0,
      game_state: 'allocating',
      group_results: [],
      members: ['1', '2'],
      now_round: 1,
      redo_count: 0
    },
    group_id: '0',
    inf_redo: false,
    page: 'instruction',
    point: 0,
    role: 'proposer',
    pushState
  }

  afterEach(() => {
    hooks.useStore.restore()
  })

  test('locales invalid', () => {
    sinon.stub(hooks, 'useStore').returns({
      ...default_state,
      locales: undefined
    })
    renderer.create(<Experiment />)
  })

  test('group invalid', () => {
    sinon.stub(hooks, 'useStore').returns({
      ...default_state,
      group: undefined
    })
    renderer.create(<Experiment />)
  })
})
