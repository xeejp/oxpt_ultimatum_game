import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

import i18nInstance from '../i18n'
import { fromCamelToSnake } from '../util'

const AllocatePieChart = ({ userAllo, enemyAllo, enemy }) => {
  const { locales } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const variablesObject = () => {
    return typeof (t('variables')) === 'object' ? t('variables') : {}
  }

  const plural = (n, unit) => {
    if (n > 1) {
      return fromCamelToSnake(unit) + '_m'
    } else {
      return fromCamelToSnake(unit) + '_s'
    }
  }

  if (!(locales))
    return <></>

  return (
    <HighchartsReact
      options={{
        title: {
          text: ''
        },
        tooltip: {
          enabled: false
        },
        plotOptions: {
          pie: {
            size: '80%',
            dataLabels: {
              enabled: true,
              crop: false,
              distance: -50,
            }
          }
        },
        series: [
          {
            type: 'pie',
            data: [
              {
                name: t(plural(userAllo, 'guest.experiment.allocating.user_allo_01'), { ...variablesObject(), user_allo: userAllo }),
                y: userAllo,
                color: '#f7a35c'
              },
              {
                name: t(plural(enemyAllo, 'guest.experiment.allocating.enemy_allo_01'), { ...variablesObject(), enemy_allo: enemyAllo, enemy: enemy }),
                y: enemyAllo,
                color: '#7cb5ec'
              }
            ]

          }
        ],
        credits: {
          enabled: false
        }
      }}
      highcharts={Highcharts}
    />
  )
}

AllocatePieChart.propTypes = {
  userAllo: PropTypes.number,
  enemyAllo: PropTypes.number,
  enemy: PropTypes.string
}

export default AllocatePieChart
