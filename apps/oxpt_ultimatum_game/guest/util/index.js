import { object } from "prop-types"

const toCamel = (str) => (
  str.replace(/([-_]\w)/g, g => g[1].toUpperCase())
)

const toSnake = (str) => (
  str.replace(/[A-Z]/g, g => `_${g[0].toLowerCase()}`)
)

const isNumber = function(value) {
  return ((typeof value === 'number') && (isFinite(value)))
}

export const fromSnakeToCamel = (obj) => {
  if (typeof obj !== 'object' || obj === null) {
    return obj
  } else {
    const result = {}
    Object.keys(obj).forEach(key => {
      if (obj[key] instanceof Object && !(obj[key] instanceof Array) && key !== 'locales' && key !== 'players') {
        result[toCamel(key)] = fromSnakeToCamel(obj[key])
      } else {
        result[toCamel(key)] = obj[key]
      }
    })
    return result
  }
}

export const fromCamelToSnake = (obj) => {
  if (typeof obj !== 'object' || obj === null) {
    return obj
  } else {
    const result = {}
    Object.keys(obj).forEach(key => {
      if (obj[key] instanceof Object && !(obj[key] instanceof Array) && key !== 'localesTemp') {
        result[toSnake(key)] = fromCamelToSnake(obj[key])
      } else {
        result[toSnake(key)] = obj[key]
      }
    })
    return result
  }
}
