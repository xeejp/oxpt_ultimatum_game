import { useSelector, useDispatch } from 'react-redux'
import * as Action from './index'

export const useStore = () => {
  const dispatch = useDispatch()
  const selector = (state) => state

  return {
    ...useSelector(selector),
    pushState: ({ event, payload, callback }) => dispatch(Action.pushState({ event, payload, callback })),
    requestState: (args) => dispatch(Action.requestState({ ...args, dispatch }))
  }
}
