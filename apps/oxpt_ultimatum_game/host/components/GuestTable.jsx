import React, { useReducer, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Grid from '@material-ui/core/Grid'

import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import TablePagination from '@material-ui/core/TablePagination'
import TableContainer from '@material-ui/core/TableContainer'
import Link from '@material-ui/core/Link'
import Paper from '@material-ui/core/Paper'
import { grey } from '@material-ui/core/colors'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  items: {
    padding: theme.spacing(1)
  },
  expand: {
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  head: {
    backgroundColor: grey[400],
    color: theme.palette.getContrastText(grey[400]),
  },
  blue: {
    color: theme.palette.primary.main,
  },
  grey: {
    color: grey[600],
  }
}))

const GuestTableHead = ({ order, orderBy, handleClick }) => {
  const classes = useStyles()
  const { page, players } = useStore()

  const playerNum = Object.keys(players).length
  const readNum = Object.keys(players).filter(key => players[key].read).length
  const finishedNum = Object.keys(players).filter(key => players[key].status === 'finished' || players[key].status === 'visiting').length

  const headCells = [
    { id: 'guest_id', sortable: true, numeric: false, label: <>ID<br />(N={playerNum})</> },
    { id: 'read', sortable: page === 'instruction', numeric: false, label: <>Read<br />({Math.floor(readNum / playerNum * 100)}%)</> },
    { id: 'status', sortable: false, numeric: false, label: 'Status' },
    { id: 'value', sortable: page === 'experiment', numeric: false, label: 'Value' },
    { id: 'finished', sortable: false, numeric: false, label: <>Finished<br />({Math.floor(finishedNum / playerNum * 100)}%)</> }
  ]

  const handleClickHead = headCell => () => {
    handleClick(headCell)
  }

  return (
    <TableHead className={classes.head}>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            {headCell.sortable
              ? <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={handleClickHead(headCell)}
              >
                {headCell.label}
              </TableSortLabel>
              : headCell.label}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  )
}

const GuestRow = ({ player, id }) => {
  const classes = useStyles()

  const [t] = useTranslation('translations', { i18nInstance })
  const { page, guestGameId, gameRounds, dummyGuestList } = useStore()

  const handleGuestClick = () => {
    window.open(`/game/${guestGameId}/${id}`)
  }

  const showPlayerStatus = (page) => {
    let playerStatus
    if (page === 'instruction' || page === 'result') {
      playerStatus = page
    } else {
      playerStatus = player.status
    }
    return t('host.guest_table.status.' + playerStatus + '_01')
  }

  return (
    <TableRow>
      <TableCell>
        <Typography>
          <Link href="#" onClick={handleGuestClick} className={dummyGuestList.includes(id) ? classes.grey : classes.blue}>
            { id.slice(0, 8) }
          </Link>
        </Typography>
      </TableCell>
      <TableCell>
        {page === 'instruction' ? player.read ? 'read' : 'reading' : null}
      </TableCell>
      <TableCell>
        {showPlayerStatus(page)}
      </TableCell>
      <TableCell>
        {(page === 'experiment' || page === 'result') ? player.round + '/' + gameRounds : null}
      </TableCell>
      <TableCell>
        {(page === 'experiment' || page === 'result') ? (player.status === 'finished' || player.status === 'visiting') ? t('host.guest_table.status.finished_01') : t('host.guest_table.status.playing_01') : null }
      </TableCell>
    </TableRow>
  )
}

const GuestTable = () => {
  const classes = useStyles()
  const { locales, players } = useStore()

  const [, i18n] = useTranslation('host', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  const [order, setOrder] = useState('asc')
  const [orderBy, setOrderBy] = useState('guest_id')
  const [tablePage, setTablePage] = React.useState(0)
  const [rowsPerPage, setRowPerPage] = useState(5)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales))
    return <></>

  const handleClickHead = headCell => {
    const isAsc = orderBy === headCell.id && order === 'asc'
    setOrder(isAsc ? 'desc' : 'asc')
    setOrderBy(headCell.id)
  }

  const descendingComparator = (key1, key2) => {
    const player1 = players[key1]
    const player2 = players[key2]

    switch (orderBy) {
      case 'guest_id':
        if (key1 < key2) return -1
        if (key1 > key2) return 1
        return 0
      case 'read':
        if (player1.read === player2.read) return 0
        if (player1.read) return -1
        return 1
      case 'status':
        return 0
      case 'value':
        if (player1.value < player2.value) return -1
        if (player1.value > player2.value) return 1
        return 0
      case 'finished':
        return 0
      default:
        return 0
    }
  }

  const sortGuest = (keys) => {
    const comparator = order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy)

    return keys.sort(comparator)
  }

  const handleChangePage = (_, newPage) => {
    setTablePage(newPage)
  }

  const handleChangeRowsPerPage = e => {
    setRowPerPage(parseInt(e.target.value, 10))
    setTablePage(0)
  }

  return (
    <Grid item xs={12} className={classes.items}>
      <TableContainer component={Paper}>
        <Table size="small">
          <GuestTableHead
            order={order}
            orderBy={orderBy}
            handleClick={handleClickHead}
          />
          <TableBody>
            {sortGuest(Object.keys(players)).slice(tablePage * rowsPerPage, tablePage * rowsPerPage + rowsPerPage).map(key =>
              <GuestRow
                key={key}
                id={key}
                player={players[key]}
              />
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={Object.keys(players).length}
        rowsPerPage={rowsPerPage}
        page={tablePage}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Grid>
  )
}

GuestRow.propTypes = {
  player: PropTypes.object,
  id: PropTypes.string,
  t: PropTypes.func
}
GuestTableHead.propTypes = {
  order: PropTypes.string,
  orderBy: PropTypes.string,
  handleClick: PropTypes.func
}

export default GuestTable
