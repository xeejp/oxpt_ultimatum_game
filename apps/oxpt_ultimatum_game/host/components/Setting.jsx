import React, { useReducer, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Box from '@material-ui/core/Box'

import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import TextField from '@material-ui/core/TextField'
import Switch from '@material-ui/core/Switch'

import Add from '@material-ui/icons/Add'
import Remove from '@material-ui/icons/Remove'
import Translate from '@material-ui/icons/Translate'
import Settings from '@material-ui/icons/Settings'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  root: {
    padding: 0,
    height: '100vh',
    overflow: 'auto',
    margin: theme.spacing(1)
  },
  dialog: {
    height: '80vh',
  },
  dialogTitle: {
    margin: 0,
    padding: 0
  },
  formControl: {
    margin: theme.spacing(1),
  },
  group: {
    margin: theme.spacing(1, 0),
  },
}))

const Setting = () => {
  const classes = useStyles()
  const { locales, page, gameRounds, gameRedo, infRedo, initialAmount, requestState } = useStore()

  if (!(locales))
    return <></>

  const languageVariablesObject = (key) => {
    return (typeof locales[key].translations.variables) === 'object' ? locales[key].translations.variables : {}
  }

  const [gameRoundsTemp, setGameRoundsTemp] = useState(gameRounds)
  const [infRedoTemp, setInfRedoTemp] = useState(infRedo)
  const [gameRedoTemp, setGameRedoTemp] = useState(gameRedo)
  const [initialAmountTemp, setInitialAmountTemp] = useState(initialAmount)
  const langKeys = Object.keys(locales)
  const initUnits = langKeys.reduce((units, key) => ({
    ...units,
    [key]: languageVariablesObject(key)
  }), {})
  const [open, setOpen] = useState(page === 'instruction')
  var localesTempStr = JSON.stringify(initUnits)
  var localesTemp = JSON.parse(localesTempStr)

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)
  const [value, setValue] = React.useState(0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const handleRoundsDec = () => {
    if (gameRoundsTemp >= 2) setGameRoundsTemp(gameRoundsTemp - 1)
  }

  const handleRoundsInc = () => {
    setGameRoundsTemp(gameRoundsTemp + 1)
  }

  const handleRedoDec = () => {
    if (gameRedoTemp >= 1) setGameRedoTemp(gameRedoTemp - 1)
  }

  const handleRedoInc = () => {
    setGameRedoTemp(gameRedoTemp + 1)
  }

  const handleChangeInfRedo = () => {
    setInfRedoTemp(!infRedoTemp)
  }

  const handleInitialAmount = e => {
    setInitialAmountTemp(parseInt(e.target.value))
  }

  const handleVariableUnits = (obj, e) => {
    var tempvar = obj.var
    localesTemp[obj.lang][tempvar] = e.target.value
  }

  const handleOnCancel = () => {
    setGameRoundsTemp(gameRounds)
    setInfRedoTemp(infRedo)
    setGameRedoTemp(gameRedo)
    setOpen(false)
  }

  const handleOnSend = () => {
    requestState({
      event: 'setting',
      payload: {
        game_rounds: gameRoundsTemp,
        inf_redo: infRedoTemp,
        game_redo: gameRedoTemp,
        initial_amount: initialAmountTemp,
        locales_temp: localesTemp
      },
      successCallback: () => {},
      timeoutCallback: () => {}
    })
    setOpen(false)
  }

  const plural = (n, unit) => {
    if (n > 1) {
      return unit + '_m'
    } else {
      return unit + '_s'
    }
  }

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  const TabPanel = (props) => {
    const { children, value, index, ...other } = props

    return (
      <Typography
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-force-tabpanel-${index}`}
        aria-labelledby={`scrollable-force-tab-${index}`}
        {...other}
      >
        {value === index && <Box p={3}>{children}</Box>}
      </Typography>
    )
  }

  const a11yProps = (index) => {
    return {
      id: `scrollable-force-tab-${index}`,
      'aria-controls': `scrollable-force-tabpanel-${index}`,
    }
  }

  return (
    <div
      className={classes.root}
    >
      <Grid item xs={12}>
        <IconButton color="primary" aria-label={t('host.setting.title_01')} disabled={page !== 'instruction'} onClick={setOpen.bind(null, true)}>
          <Settings fontSize="large" />
        </IconButton>
        <Dialog onClose={setOpen.bind(null, false)} open={open} fullWidth={true} maxWidth="md">
          <DialogTitle className={classes.dialogTitle}>
            <AppBar position="relative" color="default">
              <Tabs
                value={value}
                onChange={handleChange}
                variant="scrollable"
                scrollButtons="on"
                indicatorColor="primary"
                textColor="primary"
                aria-label="scrollable force tabs"
              >
                <Tab label={t('host.setting.game.title_01')} icon={<Settings />} {...a11yProps(0)} />
                <Tab label={t('host.setting.language.title_01')} icon={<Translate />} {...a11yProps(1)} />
              </Tabs>
            </AppBar>
          </DialogTitle>
          <DialogContent className={classes.dialog}>
            <TabPanel value={value} index={0}>
              <TableContainer component={Paper}>
                <Table aria-label="setting table" size="small">
                  <TableHead>
                    <TableRow>
                      <TableCell align="left">{t('guest.experiment.info_table.header_item_01')}</TableCell>
                      <TableCell align="center">{t('guest.experiment.info_table.header_info_01')}</TableCell>
                      <TableCell align="center">{t('host.setting.game.setting_01')}</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow>
                      <TableCell component="th" scope="row" align="left">
                        {t('host.setting.game.num_of_round_01')}
                      </TableCell>
                      <TableCell scope="row" align="right">
                        {t(plural(gameRoundsTemp, 'host.setting.game.times_01'), { num_of_times: gameRoundsTemp })}<br />({t('host.setting.game.num_of_role_change_01')}{t(plural(gameRoundsTemp - 1, 'host.setting.game.times_01'), { num_of_times: gameRoundsTemp - 1 })})

                      </TableCell>
                      <TableCell align="center">
                        <IconButton
                          color="primary"
                          aria-label="decrease round"
                          onClick={handleRoundsDec}
                          disabled={gameRoundsTemp <= 1}
                        >
                          <Remove />
                        </IconButton>
                        <IconButton
                          color="secondary"
                          aria-label="increase round"
                          onClick={handleRoundsInc}
                        >
                          <Add />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell component="th" scope="row" align="left">
                        {t('host.setting.game.num_of_redo_01')}
                      </TableCell>
                      <TableCell scope="row" align="right">
                        {t(plural(gameRedoTemp, 'host.setting.game.times_01'), { num_of_times: infRedoTemp ? t('host.setting.game.infinite_redo_setting_01') : gameRedoTemp })}
                      </TableCell>
                      <TableCell align="center">
                        <Typography component="div">
                          <Grid component="label" container alignItems="center" justify="center" spacing={1}>
                            <Grid item>{t('host.setting.game.finite_redo_setting_01')}</Grid>
                            <Grid item>
                              <Switch
                                checked={infRedoTemp}
                                onChange={handleChangeInfRedo}
                                name="infRedoTemp"
                                color="primary"
                              />
                            </Grid>
                            <Grid item>{t('host.setting.game.infinite_redo_setting_01')}</Grid>
                          </Grid>
                        </Typography>
                      </TableCell>
                    </TableRow>
                    {infRedoTemp ? null
                      : <TableRow>
                        <TableCell component="th" scope="row" align="left">
                        </TableCell>
                        <TableCell scope="row" align="right">
                        </TableCell>
                        <TableCell align="center">
                          <IconButton
                            color="primary"
                            aria-label="decrease suggest times"
                            onClick={handleRedoDec}
                            disabled={gameRedoTemp <= 0 || infRedoTemp}
                          >
                            <Remove />
                          </IconButton>
                          <IconButton
                            color="secondary"
                            aria-label="increase suggest times"
                            onClick={handleRedoInc}
                            disabled={infRedoTemp}
                          >
                            <Add />
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    }
                    <TableRow>
                      <TableCell component="th" scope="row" align="left">
                        {t('host.setting.game.initial_amount_setting_01')}
                      </TableCell>
                      <TableCell scope="row" align="right">
                        {initialAmountTemp}
                      </TableCell>
                      <TableCell align="center">
                        <TextField
                          type='number'
                          value={initialAmountTemp}
                          onChange={handleInitialAmount}
                        />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </TabPanel>
            <TabPanel value={value} index={1}>
              <TableContainer component={Paper}>
                <Table aria-label="setting table" size="small">
                  <TableHead>
                    <TableRow>
                      {Object.keys(initUnits).map(k => {
                        return <TableCell align="center" key={k}>
                          {t('host.setting.language.' + k + '_01')}
                        </TableCell>
                      })}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {Object.keys(initUnits['en']).map((key, index) => {
                      return <TableRow key={key + '_' + index}>
                        {Object.keys(initUnits).map(k => {
                          return <TableCell align="center" key={key + '_' + index + '_' + k}>
                            <TextField
                              key={key + '_' + index + '_' + k}
                              defaultValue={Object.values(initUnits[k])[index] || null}
                              onChange={handleVariableUnits.bind(null, { lang: k, var: key })}
                            />
                          </TableCell>
                        })}
                      </TableRow>
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
            </TabPanel>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleOnCancel}>
              {t('host.setting.cancel_01')}
            </Button>
            <Button onClick={handleOnSend} variant="contained" color="primary" disabled={page !== 'instruction' || initialAmountTemp <= 0}>
              {t('host.setting.send_01')}
            </Button>
          </DialogActions>
        </Dialog>
      </Grid>
    </div>
  )
}

Setting.propTypes = {
  value: PropTypes.number,
  index: PropTypes.number
}

export default Setting
