import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Grid from '@material-ui/core/Grid'

import CircularProgress from '@material-ui/core/CircularProgress'

import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'

import i18nInstance from '../i18n'

import SmsIcon from '@material-ui/icons/Sms'
import ShowChartIcon from '@material-ui/icons/ShowChart'
import GavelIcon from '@material-ui/icons/Gavel'
import PropTypes from 'prop-types'
import Avatar from '@material-ui/core/Avatar'
import { grey } from '@material-ui/core/colors'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatarOn: {
    color: theme.palette.getContrastText(theme.palette.primary.main),
    backgroundColor: theme.palette.primary.main,
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 11,
    width: 50,
    height: 50,
    zIndex: 1,
  },
  avatarOff: {
    color: theme.palette.common.white,
    backgroundColor: grey[400],
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 11,
    width: 50,
    height: 50,
    zIndex: 1,
  },
  labelOn: {
    color: theme.palette.primary.main,
  },
  labelOff: {
    color: grey[400],
  },
  items: {
    padding: theme.spacing(1),
  },
  button: {
    padding: theme.spacing(2),
  }
}))

export default () => {
  const classes = useStyles()
  const { page, locales, requestState } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  const pages = ['instruction', 'experiment', 'result']
  const activeStep = pages.findIndex(p => p === page)

  const [isdisableButton, setIsdisableButton] = useState(false)
  const [waitNext, setWaitNext] = useState(false)
  const [waitBack, setWaitBack] = useState(false)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const handleBack = () => {
    if (activeStep > 0) {
      setWaitBack(true)
      dispatchChangePage(activeStep - 1)
    }
  }

  const handleNext = () => {
    if (activeStep < pages.length - 1) {
      setWaitNext(true)
      dispatchChangePage(activeStep + 1)
    }
  }

  const handleStep = step => () => {
    setWaitBack(true)
    setWaitNext(true)
    dispatchChangePage(step)
  }

  const dispatchChangePage = p => {
    setIsdisableButton(true)
    if (p >= 0 && p < pages.length) {
      let changePage = pages[p]
      requestState({ event: 'change page', payload: changePage, successCallback: callbackChangePage, timeoutCallback: callbackChangePage })
    }
  }

  const callbackChangePage = (errorcode) => {
    setWaitBack(false)
    setWaitNext(false)
    setIsdisableButton(false)
  }

  const ColorlibStepIcon = (step) => () => {
    const icons = {
      0: <SmsIcon />,
      1: <GavelIcon />,
      2: <ShowChartIcon />,
    }

    return (
      <Avatar className={(step !== activeStep || isdisableButton) ? classes.avatarOff : classes.avatarOn }>
        {icons[String(step)]}
      </Avatar>
    )
  }

  ColorlibStepIcon.propTypes = {
    icon: PropTypes.node,
  }

  if (!(locales))
    return <></>

  return (
    <Grid
      container
      direction="column"
      justify="center"
      alignItems="stretch"
    >
      <Grid item xs={12}>
        <Stepper alternativeLabel nonLinear activeStep={activeStep}>
          {pages.map((page, index) =>
            <Step key={page}>
              <StepLabel StepIconComponent={ColorlibStepIcon(index)} onClick={(isdisableButton) ? null : handleStep(index)}>
                <Typography variant="body2" className={(index !== activeStep || isdisableButton) ? classes.labelOff : classes.labelOn }>
                  {t(`host.stepper.${page}_page_01`)}
                </Typography>
              </StepLabel>
            </Step>
          )}
        </Stepper>
      </Grid>
      <Grid item xs={12}>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
        >
          <Grid item xs={6} sm={3} className={classes.items}>
            <Button
              disabled={activeStep === 0 || isdisableButton}
              onClick={handleBack}
              variant="outlined"
              fullWidth
              className={classes.button}
            >
              {waitBack && <CircularProgress size={18} className={classes.buttonProgress}/>}
              {!waitBack && t('host.stepper.back_01')}
            </Button>
          </Grid>
          <Grid item xs={6} sm={3} className={classes.items}>
            <Button
              variant='contained'
              color='primary'
              disabled={activeStep === pages.length - 1 || isdisableButton}
              onClick={handleNext}
              fullWidth
              className={classes.button}
            >
              {waitNext && <CircularProgress size={24} className={classes.buttonProgress}/>}
              {!waitNext && t('host.stepper.next_01')}
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}
