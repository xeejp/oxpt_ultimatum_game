defmodule Oxpt.UltimatumGame.Events do
  @moduledoc """
  Documentation for Oxpt.UltimatumGame.Events

  gameで使うイベントを定義する
  """

  defmodule UpdateStateAll do
    @moduledoc """
    すべてのplayer_socketがsubscribeしておくべきイベント。
    全員に対しあるstateを送りたいときに使う

    """
    defstruct [:game_id, event: "update_state", state: %{}]
  end

  defmodule UpdateState do
    @moduledoc """
    guest_idで指定されたゲストのplayer_socketがsubscribeしておくべきイベント。
    そのゲストに対しあるstateを送りたいときに使う

    """
    defstruct [:guest_id, :game_id, event: "update_state", state: %{}]
  end

  defmodule UpdateStateGroup do
    defstruct [:group_member_id, :game_id, event: "update_state", state: %{}]
  end

  defmodule FetchState do
    @moduledoc """
    stateを管理しているオートマトンがsubscribeしておくべきイベント。
    player_socketがspawnした時にDispatchし、クライアントにstateを送る

    """
    defstruct [:game_id, :guest_id]
  end

  defmodule Read do
    defstruct [:game_id, :guest_id]
  end

  defmodule ChangePage do
    defstruct [:game_id, :request_id, :page]
  end

  defmodule ChangeSetting do
    defstruct [:game_id, :request_id, :payload]
  end

  defmodule ChangeAlloTemp do
    defstruct [:game_id, :guest_id, :request_id, :allo_temp]
  end

  defmodule FinishAllocating do
    defstruct [:game_id, :guest_id, :request_id, :value]
  end

  defmodule Response do
    defstruct [:game_id, :guest_id, :request_id, :accept]
  end

  defmodule ResetResponse do
    defstruct [:game_id, :request_id, :guest_id]
  end

  defmodule RedoAllocating do
    defstruct [:game_id, :guest_id, :request_id]
  end
end
