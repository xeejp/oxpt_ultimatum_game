defmodule Oxpt.UltimatumGame.Locales do
  def get do
    %{
      en: %{
        translations: %{
          variables: %{
            visitor_01: "visitor",
            proposer_01: "proposer",
            responder_01: "responder",
            point_unit_01_s: "point",
            point_unit_01_m: "points"
          },
          host: %{
            title_01: "Ultimatum Game",
            stepper: %{
              instruction_page_01: "Instruction",
              experiment_page_01: "Experiment",
              result_page_01: "Result",
              back_01: "Back",
              next_01: "Next"
            },
            setting: %{
              title_01: "Settings",
              send_01: "Send",
              cancel_01: "Cancel",
              language: %{
                title_01: "Language settings",
                en_01: "English",
                ja_01: "Japanese"
              },
              game: %{
                title_01: "Game settings",
                setting_01: "Setting",
                num_of_round_01: "Numbers of round",
                times_01_s: "{{num_of_times}} time",
                times_01_m: "{{num_of_times}} times",
                num_of_role_change_01: "Role change: ",
                num_of_redo_01: "Proposable times",
                finite_redo_setting_01: "Limited times",
                infinite_redo_setting_01: "Unlimited times",
                initial_amount_setting_01: "Initial amount"
              }
            },
            guest_table: %{
              status: %{
                instruction_01: "instruction",
                result_01: "result",
                allocating_01: "allocating",
                waiting_01: "waiting",
                responding_01: "responding",
                finished_01: "finished",
                playing_01: "playing",
                visiting_01: "-"
              }
            }
          },
          guest: %{
            instruction: %{
              instruction_title_01: "Instruction",
              next_01: "next",
              back_01: "back",
              instructions_01_s: [
                "<p>In this game, the prize will be distributed by a pair of two people.</p>",
                "<p>We call one participant as the {{proposer_01}} and the other as the {{responder_01}}.</p>",
                "<p>Assume that the participant who became the {{proposer_01}} was given {{initial_amount}} {{point_unit_01_s}}.</p><p>The {{proposer_01}} can destribute this {{initial_amount}} {{point_unit_01_s}} between the {{responder_01}} and the {{proposer_01}}.</p><p>The {{proposer_01}} proposes a decision to distribute to the {{responder_01}}.</p>",
                "<p>The {{responder_01}} will review the proposal and decide whether or not to accept it.</p><p>When the {{responder_01}} accepts the proposal, {{initial_amount}} {{point_unit_01_s}} will be split into two, as suggested by the {{proposer_01}}.</p><p>When the {{responder_01}} rejects the offer, both will be assigned 0 {{point_unit_01_s}}.</p>",
                "<p>If you have any questions, we will receive it right away.</p><p>Do not ask questions or talk to other subjects during the game.</p><p>Also, do not look at the screens of other participant.</p>"
              ],
              instructions_01_m: [
                "<p>In this game, the prize will be distributed by a pair of two people.</p>",
                "<p>We call one participant as the {{proposer_01}} and the other as the {{responder_01}}.</p>",
                "<p>Assume that the participant who became the {{proposer_01}} was given {{initial_amount}} {{point_unit_01_m}}.</p><p>The {{proposer_01}} can destribute this {{initial_amount}} {{point_unit_01_m}} between the {{responder_01}} and the {{proposer_01}}.</p><p>The {{proposer_01}} proposes a decision to distribute to the {{responder_01}}.</p>",
                "<p>The {{responder_01}} will review the proposal and decide whether or not to accept it.</p><p>When the {{responder_01}} accepts the proposal, {{initial_amount}} {{point_unit_01_m}} will be split into two, as suggested by the {{proposer_01}}.</p><p>When the {{responder_01}} rejects the offer, both will be assigned 0 {{point_unit_01_s}}.</p>",
                "<p>If you have any questions, we will receive it right away.</p><p>Do not ask questions or talk to other subjects during the game.</p><p>Also, do not look at the screens of other participant.</p>"
              ]
            },
            experiment: %{
              info_table: %{
                header_item_01: "Items",
                header_info_01: "Information",
                header_round_01: "Rounds",
                header_role_change_01: "Role change times",
                header_suggestion_01: "Re-Suggestion times",
                header_point_01: "Points",
                round_view_now_01_s: "Now {{now_round}} time / ",
                round_view_now_01_m: "Now {{now_round}} times / ",
                round_view_total_01_s: "Total {{game_rounds}} time",
                round_view_total_01_m: "Total {{game_rounds}} times",
                final_round_01: "This is the final round.",
                role_change_view_01_s: "{{round}} time left.",
                role_change_view_01_m: "{{round}} times left.",
                inf_redo_01: "infinite times",
                suggestion_view_now_01_s: "Now {{redo_count}} time / ",
                suggestion_view_now_01_m: "Now {{redo_count}} times / ",
                suggestion_view_total_01_s: "Total {{game_redo}} time",
                suggestion_view_total_01_m: "Total {{game_redo}} times",
                final_suggestion_01: "This is the final propose."
              },
              allocating: %{
                user_role_01: "You are the {{role}}.",
                user_allocate_01: "Please move the slide bar to determine the allocation amount.",
                enemy_allocate_01: "Please wait until the {{enemy}} made the decision.",
                user_allo_01_s: "Distribution to you: {{user_allo}} {{point_unit_01_s}}",
                user_allo_01_m: "Distribution to you: {{user_allo}} {{point_unit_01_m}}",
                enemy_allo_01_s:
                  "Distribution to the {{enemy}}: {{enemy_allo}} {{point_unit_01_s}}",
                enemy_allo_01_m:
                  "Distribution to the {{enemy}}: {{enemy_allo}} {{point_unit_01_m}}",
                allocate_confirm_user_01_s: "Distribute {{user_allo}} {{point_unit_01_s}} ",
                allocate_confirm_user_01_m: "Distribute {{user_allo}} {{point_unit_01_m}} ",
                allocate_confirm_enemy_01_s: "to you and {{enemy_allo}} {{point_unit_01_s}} ",
                allocate_confirm_enemy_01_m: "to you and {{enemy_allo}} {{point_unit_01_m}} ",
                allocate_confirm_initial_01_s:
                  "to the opponent out of the initial holding amount of {{initial_amount}} {{point_unit_01_s}}. Are you sure?",
                allocate_confirm_initial_01_m:
                  "to the opponent out of the initial holding amount of {{initial_amount}} {{point_unit_01_m}}. Are you sure?",
                propose_01: "Propose",
                cancel_01: "Cancel",
                next_01: "Next"
              },
              judging: %{
                send_wait_01: "Proposed. Please wait until the {{enemy}} made the decision.",
                judging_notice_01:
                  "The {{proposer_01}} propose you. Please reply to the decision.",
                suggestion_01:
                  "The {{proposer_01}} has proposed as described above. Answer either accept or reject.",
                accept_01: "Accept",
                reject_01: "Reject"
              },
              response: %{
                response_OK_01: "Your propose was accepted by the {{responder_01}}.",
                response_NG_01: "Your propose was rejected by the {{responder_01}}.",
                response_REDO_01:
                  "Your propose was rejected by the {{responder_01}}. Please propose again.",
                close_01: "Close"
              },
              finished: %{
                user_role_mini_01: "{{role}}",
                wait_end_01: "Please wait for this game to end.",
                finished_01: "Your turn was finished. Please wait for this game to end."
              },
              error: %{
                cant_join_01: "Could not join the game. Please wait for this game to end."
              }
            },
            result: %{
              result_graph: %{
                title_01: "The result greph",
                allo_point_01: "Points for {{proposer_01}}",
                chart_count_01: "Times",
                result_accept_01: "Accepted",
                result_reject_01: "Rejected",
                chart_round_01: "Rounds: {{chart_round}}",
                all_round_01: "All rounds"
              },
              round_result: %{
                title_01: "The details of each round.",
                your_role_01: "Your role.",
                distribution_ratio_01: "Distribution ratio",
                result_page_01: "Result"
              }
            }
          }
        }
      },
      ja: %{
        translations: %{
          variables: %{
            visitor_01: "見学者",
            proposer_01: "提案者",
            responder_01: "応答者",
            point_unit_01_s: "ポイント",
            point_unit_01_m: "ポイント"
          },
          host: %{
            title_01: "最後通牒ゲーム",
            stepper: %{
              instruction_page_01: "説明",
              experiment_page_01: "実験",
              result_page_01: "結果",
              back_01: "戻る",
              next_01: "次へ"
            },
            setting: %{
              title_01: "設定",
              send_01: "送信",
              cancel_01: "キャンセル",
              language: %{
                title_01: "言語設定",
                en_01: "英語",
                ja_01: "日本語"
              },
              game: %{
                title_01: "ゲーム設定",
                setting_01: "設定",
                num_of_round_01: "ラウンド数",
                times_01_s: "{{num_of_times}}回",
                times_01_m: "{{num_of_times}}回",
                num_of_role_change_01: "役割交代",
                num_of_redo_01: "再提案可能回数",
                finite_redo_setting_01: "有限",
                infinite_redo_setting_01: "無限",
                initial_amount_setting_01: "初期保有額"
              }
            },
            guest_table: %{
              status: %{
                instruction_01: "説明",
                result_01: "結果",
                allocating_01: "分配中",
                waiting_01: "待機中",
                responding_01: "反応中",
                finished_01: "終了",
                playing_01: "実験中",
                visiting_01: "-"
              }
            }
          },
          guest: %{
            instruction: %{
              instruction_title_01: "ゲームの説明",
              next_01: "次へ",
              back_01: "戻る",
              instructions_01_s: [
                "<p>この実験では、2人1組で賞金の配分を行っていただきます。</p>",
                "<p>2人のうち1人を「{{proposer_01}}」、もう1人を「{{responder_01}}」と呼びます。</p>",
                "<p>{{proposer_01}}は{{initial_amount}}{{point_unit_01_s}}の賞金が与えられたと想定してください。</p><p>{{proposer_01}}は、自分と{{responder_01}}との間でこの{{initial_amount}}{{point_unit_01_s}}を1{{point_unit_01_s}}単位で自由に分けることができます。</p><p>{{proposer_01}}は、両者への配分額を{{responder_01}}に提案します。</p>",
                "<p>{{responder_01}}は提案を確認した後、その提案を受け入れるか否かを決定します。</p><p>{{responder_01}}がその提案を受け入れた場合、{{proposer_01}}の提案通りに{{initial_amount}}{{point_unit_01_s}}が2人の間で配分されます。</p><p>{{responder_01}}がその提案を拒否した場合、2人の配分額はともに0{{point_unit_01_s}}となります。</p>",
                "<p>質問がある場合は、いま受け付けます。</p><p>実験中は質問をしたり、他の被験者と話をしてはいけません。</p><p>また、他の被験者の画面を見ないでください。</p>"
              ],
              instructions_01_m: [
                "<p>この実験では、2人1組で賞金の配分を行っていただきます。</p>",
                "<p>2人のうち1人を「{{proposer_01}}」、もう1人を「{{responder_01}}」と呼びます。</p>",
                "<p>{{proposer_01}}は{{initial_amount}}{{point_unit_01_m}}の賞金が与えられたと想定してください。</p><p>{{proposer_01}}は、自分と{{responder_01}}との間でこの{{initial_amount}}{{point_unit_01_m}}を1{{point_unit_01_s}}単位で自由に分けることができます。</p><p>{{proposer_01}}は、両者への配分額を{{responder_01}}に提案します。</p>",
                "<p>{{responder_01}}は提案を確認した後、その提案を受け入れるか否かを決定します。</p><p>{{responder_01}}がその提案を受け入れた場合、{{proposer_01}}の提案通りに{{initial_amount}}{{point_unit_01_m}}が2人の間で配分されます。</p><p>{{responder_01}}がその提案を拒否した場合、2人の配分額はともに0{{point_unit_01_s}}となります。</p>",
                "<p>質問がある場合は、いま受け付けます。</p><p>実験中は質問をしたり、他の被験者と話をしてはいけません。</p><p>また、他の被験者の画面を見ないでください。</p>"
              ]
            },
            experiment: %{
              info_table: %{
                header_item_01: "項目",
                header_info_01: "情報",
                header_round_01: "ラウンド",
                header_role_change_01: "役割交代",
                header_suggestion_01: "再提案",
                header_point_01: "ポイント",
                round_view_now_01_s: "現在{{now_round}}回目 / ",
                round_view_now_01_m: "現在{{now_round}}回目 / ",
                round_view_total_01_s: "全{{game_rounds}}回",
                round_view_total_01_m: "全{{game_rounds}}回",
                final_round_01: "これが最後のラウンド",
                role_change_view_01_s: "残り{{round}}回",
                role_change_view_01_m: "残り{{round}}回",
                inf_redo_01: "何回でも可能",
                suggestion_view_now_01_s: "現在{{redo_count}}回目 / ",
                suggestion_view_now_01_m: "現在{{redo_count}}回目 / ",
                suggestion_view_total_01_s: "全{{game_redo}}回",
                suggestion_view_total_01_m: "全{{game_redo}}回",
                final_suggestion_01: "これが最後の提案"
              },
              allocating: %{
                user_role_01: "あなたは{{role}}です。",
                user_allocate_01: "スライドバーを動かして、配分額を決定してください",
                enemy_allocate_01: "{{enemy}}が配分額を決定するまで、しばらくお待ちください。",
                user_allo_01_s: "あなたへの配分: {{user_allo}}{{point_unit_01_s}}",
                user_allo_01_m: "あなたへの配分: {{user_allo}}{{point_unit_01_m}}",
                enemy_allo_01_s: "{{enemy}}への配分: {{enemy_allo}}{{point_unit_01_s}}",
                enemy_allo_01_m: "{{enemy}}への配分: {{enemy_allo}}{{point_unit_01_m}}",
                allocate_confirm_user_01_s: "を、あなたへ{{user_allo}}{{point_unit_01_s}}",
                allocate_confirm_user_01_m: "を、あなたへ{{user_allo}}{{point_unit_01_m}}",
                allocate_confirm_enemy_01_s:
                  "、相手に{{enemy_allo}}{{point_unit_01_s}}で配分するように提案します。よろしいですか。",
                allocate_confirm_enemy_01_m:
                  "、相手に{{enemy_allo}}{{point_unit_01_m}}で配分するように提案します。よろしいですか。",
                allocate_confirm_initial_01_s: "初期保有額{{initial_amount}}{{point_unit_01_s}}",
                allocate_confirm_initial_01_m: "初期保有額{{initial_amount}}{{point_unit_01_m}}",
                propose_01: "提案する",
                cancel_01: "キャンセル",
                next_01: "次へ"
              },
              judging: %{
                send_wait_01: "提案しました。{{responder_01}}の回答をお待ちください。",
                judging_notice_01: "{{proposer_01}}から提案されました。回答を選択してください。",
                suggestion_01: "{{proposer_01}}が上記のように提案してきました。同意か拒否のいずれかを回答してください。",
                accept_01: "同意する",
                reject_01: "拒否する"
              },
              response: %{
                response_OK_01: "提案が受理されました。",
                response_NG_01: "提案が拒否されました。",
                response_REDO_01: "提案が拒否されました。再度提案を行ってください。",
                close_01: "閉じる"
              },
              finished: %{
                user_role_mini_01: "{{role}}",
                wait_end_01: "終了待機",
                finished_01: "あなたのゲームは終了しました。ほかのペアのゲームが終了するまでお待ちください。"
              },
              error: %{
                cant_join_01: "参加できませんでした。ゲームの終了をお待ち下さい。"
              }
            },
            result: %{
              result_graph: %{
                title_01: "結果グラフ",
                allo_point_01: "提案者に分配されたポイント",
                chart_count_01: "回数",
                result_accept_01: "承認",
                result_reject_01: "拒否",
                chart_round_01: "表示ラウンド: {{chart_round}}",
                all_round_01: "全ラウンド合算"
              },
              round_result: %{
                title_01: "各ラウンドの結果",
                your_role_01: "あなたの役割",
                distribution_ratio_01: "分配比",
                result_page_01: "結果"
              }
            }
          }
        }
      }
    }
  end
end
