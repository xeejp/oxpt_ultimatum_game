defmodule Oxpt.UltimatumGame.Guest do
  @moduledoc """
  Documentation for Oxpt.UltimatumGame.Guest
  """

  use Cizen.Automaton
  defstruct [:room_id]

  use Cizen.Effects
  use Cizen.Effectful
  alias Cizen.{Event, Filter}
  alias Oxpt.JoinGame
  alias Oxpt.Persistence
  alias Oxpt.Game
  alias Oxpt.Player
  alias Oxpt.Lounge.Host.AddedDummyGuest

  alias Oxpt.UltimatumGame.{Host, Locales}

  alias Oxpt.UltimatumGame.Events.{
    FetchState,
    UpdateState,
    UpdateStateAll,
    UpdateStateGroup,
    Read,
    ChangePage,
    ChangeSetting,
    ChangeAlloTemp,
    FinishAllocating,
    Response,
    ResetResponse,
    RedoAllocating
  }

  use Oxpt.Game,
    root_dir: Path.join(__ENV__.file, "../../guest") |> Path.expand()

  @impl Oxpt.Game
  def player_socket(game_id, %__MODULE__{room_id: _room_id}, guest_id) do
    %__MODULE__.PlayerSocket{game_id: game_id, guest_id: guest_id}
  end

  @impl Oxpt.Game
  def new(room_id, _params) do
    %__MODULE__{room_id: room_id}
  end

  @impl Oxpt.Game
  def metadata(),
    do: %{
      label: "label_ultimatum_game",
      category: "category_behavioural_economics"
    }

  @impl true
  def spawn(id, %__MODULE__{room_id: room_id}) do
    Persistence.Game.setup(id, room_id)

    host_game_id =
      unless Application.get_env(:oxpt, :restoring, false) do
        perform(id, %Start{saga: Host.new(room_id, game_id: id)})
      else
        nil
      end

    perform(id, %Subscribe{
      event_filter:
        Filter.new(fn
          %Event{
            body: %FetchState{game_id: ^id}
          } ->
            true

          %Event{
            body: %AddedDummyGuest{game_id: ^id}
          } ->
            true

          %Event{
            body: %ChangePage{game_id: ^id}
          } ->
            true

          %Event{
            body: %Read{game_id: ^id}
          } ->
            true

          %Event{
            body: %ChangeSetting{game_id: ^id}
          } ->
            true

          %Event{
            body: %ChangeAlloTemp{game_id: ^id}
          } ->
            true

          %Event{
            body: %FinishAllocating{game_id: ^id}
          } ->
            true

          %Event{
            body: %Response{game_id: ^id}
          } ->
            true

          %Event{
            body: %ResetResponse{game_id: ^id}
          } ->
            true

          %Event{
            body: %RedoAllocating{game_id: ^id}
          } ->
            true

          %Event{
            body: %JoinGame{game_id: ^id}
          } ->
            true
        end)
    })

    initial_state = %{
      host_game_id: host_game_id,
      host_guest_id: nil,
      guest_game_id: id,
      locales: Locales.get(),
      page: "instruction",
      players: %{},
      groups: %{},
      game_rounds: 1,
      game_redo: 0,
      inf_redo: false,
      initial_amount: 1000,
      ultimatum_results: %{},
      dummy_guest_list: []
    }

    {:loop, initial_state}
  end

  @impl true
  def yield(id, {:loop, state}) do
    event = perform(id, %Receive{})

    state = handle_event_body(id, event.body, state)
    {:loop, state}
  end

  defp handle_event_body(id, %JoinGame{guest_id: guest_id, host: true}, state) do
    perform(id, %Request{
      body: %JoinGame{game_id: state.host_game_id, guest_id: guest_id, host: true}
    })

    %{state | host_guest_id: guest_id}
  end

  defp handle_event_body(id, %JoinGame{guest_id: guest_id}, state) do
    player = new_player(state)
    new_state = put_in(state, [:players, guest_id], player)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          players: get_in(new_state, [:players])
        }
      }
    })

    new_state
  end

  defp handle_event_body(id, %FetchState{guest_id: guest_id}, state) do
    perform(id, %Dispatch{
      body: %UpdateState{
        game_id: id,
        guest_id: guest_id,
        state: state
      }
    })

    state
  end

  defp handle_event_body(id, %AddedDummyGuest{dummy_guest_list: dummy_guest_list}, state) do
    new_state = put_in(state, [:dummy_guest_list], dummy_guest_list)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          dummy_guest_list: get_in(new_state, [:dummy_guest_list])
        }
      }
    })

    new_state
  end

  defp handle_event_body(id, %Read{guest_id: guest_id}, state) do
    perform(id, %Dispatch{
      body: %UpdateState{
        game_id: id,
        guest_id: guest_id,
        state: %{
          players: %{
            guest_id => %{
              read: true,
              status: "instruction finished"
            }
          }
        }
      }
    })

    # ignored when state.host_guest_id is nil.
    perform(id, %Dispatch{
      body: %UpdateState{
        game_id: id,
        guest_id: state.host_guest_id,
        state: %{
          players: %{
            guest_id => %{
              read: true,
              status: "instruction finished"
            }
          }
        }
      }
    })

    state
    |> put_in([:players, guest_id, :read], true)
  end

  defp handle_event_body(id, %ChangePage{request_id: request_id, page: page}, state) do
    new_state =
      if state.page == "instruction" && page == "experiment" do
        # playersとgroupsとmatchedを変更
        reset(state)
      else
        state
      end
    
    new_state =
      new_state
      |> Map.put(:page, page)
      |> put_in([:player_num], Enum.count(new_state.players))

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          page: get_in(new_state, [:page]),
          players: get_in(new_state, [:players]),
          groups: get_in(new_state, [:groups]),
          matched: get_in(new_state, [:matched]),
          player_num: get_in(new_state, [:player_num])
        }
      }
    })

    response(id, request_id)

    new_state
  end

  defp handle_event_body(id, %ChangeSetting{request_id: request_id, payload: payload}, state) do
    new_state =
      state
      |> put_in([:game_rounds], payload["game_rounds"])
      |> put_in([:game_redo], payload["game_redo"])
      |> put_in([:inf_redo], payload["inf_redo"])
      |> put_in([:initial_amount], payload["initial_amount"])
      |> update_locales(payload["locales_temp"])
      |> reset

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: new_state
      }
    })

    response(id, request_id)

    new_state
  end

  defp handle_event_body(id, %ChangeAlloTemp{request_id: request_id, guest_id: guest_id, allo_temp: allo_temp}, state) do
    group_id = get_in(state, [:players, guest_id, :group_id])

    "allocating" = get_in(state, [:groups, group_id, :game_state])
    new_state = put_in(state, [:groups, group_id, :allo_temp], allo_temp)

    perform(id, %Dispatch{
      body: %UpdateStateGroup{
        game_id: id,
        group_member_id: get_in(new_state, [:groups, group_id, :members]),
        state: %{
          players: get_in(new_state, [:players]),
          groups: get_in(new_state, [:groups])
        }
      }
    })

    response(id, request_id)

    new_state
  end

  defp handle_event_body(
         id,
         %FinishAllocating{request_id: request_id, guest_id: guest_id, value: value},
         state
       ) do
    group_id = get_in(state, [:players, guest_id, :group_id])

    another_id = Enum.at(get_in(state, [:groups, group_id, :members]) -- [guest_id], 0)

    new_state =
      state
      |> put_in([:groups, group_id, :game_state], "judging")
      |> put_in([:groups, group_id, :allo_temp], value)
      |> put_in([:players, guest_id, :status], "waiting")
      |> put_in([:players, another_id, :status], "responding")

    perform(id, %Dispatch{
      body: %UpdateStateGroup{
        game_id: id,
        group_member_id: get_in(new_state, [:groups, group_id, :members]),
        state: %{
          players: get_in(new_state, [:players]),
          groups: get_in(new_state, [:groups])
        }
      }
    })

    response(id, request_id)

    new_state
  end

  defp handle_event_body(
         id,
         %Response{request_id: request_id, guest_id: guest_id, accept: accept},
         state
       ) do
    group_id = get_in(state, [:players, guest_id, :group_id])
    group = get_in(state, [:groups, group_id])
    game_rounds = state.game_rounds
    initial_amount = state.initial_amount

    allo = get_in(group, [:allo_temp])
    now_round = get_in(group, [:now_round])
    members = get_in(group, [:members])

    another_id = Enum.at(get_in(state, [:groups, group_id, :members]) -- [guest_id], 0)

    target_id =
      case members do
        [^guest_id, target_id] -> target_id
        [target_id, ^guest_id] -> target_id
      end

    "judging" = get_in(group, [:game_state])

    new_state =
      state
      |> update_in([:players, guest_id, :role], &get_next_role/1)
      |> update_in([:players, target_id, :role], &get_next_role/1)
      |> put_in([:groups, group_id, :redo_count], 0)
      |> put_in(
        [:groups, group_id, :game_state],
        case now_round < game_rounds do
          true -> "allocating"
          false -> "finished"
        end
      )
      |> put_in(
        [:groups, group_id, :now_round],
        case now_round < game_rounds do
          true -> now_round + 1
          false -> now_round
        end
      )
      |> put_in(
        [:players, guest_id, :status],
        case now_round < game_rounds do
          true -> "allocating"
          false -> "finished"
        end
      )
      |> put_in(
        [:players, another_id, :status],
        case now_round < game_rounds do
          true -> "waiting"
          false -> "finished"
        end
      )
      |> put_in(
        [:players, guest_id, :round],
        case now_round < game_rounds do
          true -> now_round + 1
          false -> now_round
        end
      )
      |> put_in(
        [:players, another_id, :round],
        case now_round < game_rounds do
          true -> now_round + 1
          false -> now_round
        end
      )
      |> put_in([:groups, group_id, :allo_temp], div(initial_amount, 2))
      |> update_in([:groups, group_id, :group_results], fn list ->
        [%{proposer: target_id, allo: allo, accepted: accept} | list]
      end)
      |> update_in([:ultimatum_results], fn ultimatum_results ->
        Map.merge(ultimatum_results, %{
          Integer.to_string(now_round) =>
            Map.merge(
              get_in(ultimatum_results, [Integer.to_string(now_round)]) || %{},
              %{
                group_id => %{
                  allo: allo,
                  accept: accept
                }
              }
            )
        })
      end)

    new_state =
      if accept do
        new_state
        |> update_in([:players, guest_id, :point], fn point ->
          case get_in(state, [:players, guest_id, :role]) == "responder" do
            true -> point + (initial_amount - allo)
            false -> point + allo
          end
        end)
        |> update_in([:players, target_id, :point], fn point ->
          case get_in(state, [:players, target_id, :role]) == "responder" do
            true -> point + (initial_amount - allo)
            false -> point + allo
          end
        end)
        |> put_in([:groups, group_id, :response], "OK")
      else
        new_state
        |> put_in([:groups, group_id, :response], "NG")
      end

    f_num =
      Map.values(new_state.players)
      |> Enum.filter(fn x -> x.status === "finished" end)
      |> length

    new_state =
      new_state
      |> put_in([:finished_num], f_num)

    perform(id, %Dispatch{
      body: %UpdateStateGroup{
        game_id: id,
        group_member_id: members,
        state: %{
          players: get_in(new_state, [:players]),
          groups: get_in(new_state, [:groups])
        }
      }
    })

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          ultimatum_results: get_in(new_state, [:ultimatum_results]),
          finished_num: get_in(new_state, [:finished_num])
        }
      }
    })

    # if Enum.all?(
    #      Map.keys(new_state.groups),
    #      &(get_in(new_state, [:groups, &1, :game_state]) == "finished")
    #    ) do
    #   perform(id, %Dispatch{
    #     body: %ChangePage{
    #       game_id: id,
    #       page: "result"
    #     }
    #   })
    # end

    response(id, request_id)

    new_state
  end

  defp handle_event_body(id, %RedoAllocating{request_id: request_id, guest_id: guest_id}, state) do
    group_id = get_in(state, [:players, guest_id, :group_id])

    another_id = Enum.at(get_in(state, [:groups, group_id, :members]) -- [guest_id], 0)

    new_state =
      state
      |> update_in([:groups, group_id, :redo_count], &(&1 + 1))
      |> put_in([:groups, group_id, :game_state], "allocating")
      |> put_in([:groups, group_id, :response], "REDO")
      |> put_in([:players, guest_id, :status], "waiting")
      |> put_in([:players, another_id, :status], "allocating")

    perform(id, %Dispatch{
      body: %UpdateStateGroup{
        game_id: id,
        group_member_id: get_in(new_state, [:groups, group_id, :members]),
        state: %{
          players: get_in(new_state, [:players]),
          groups: get_in(new_state, [:groups])
        }
      }
    })

    response(id, request_id)

    new_state
  end

  defp handle_event_body(id, %ResetResponse{request_id: request_id, guest_id: guest_id}, state) do
    group_id = get_in(state, [:players, guest_id, :group_id])

    new_state = put_in(state, [:groups, group_id, :response], "")

    perform(id, %Dispatch{
      body: %UpdateStateGroup{
        game_id: id,
        group_member_id: get_in(new_state, [:groups, group_id, :members]),
        state: %{
          players: get_in(new_state, [:players]),
          groups: get_in(new_state, [:groups])
        }
      }
    })

    response(id, request_id)

    new_state
  end

  defp match_groups(state, group_size) do
    # playersとgroupsを変更
    initial_amount = state.initial_amount
    %{players: players} = state

    groups =
      players
      # [id, id, ...]
      |> Enum.map(&elem(&1, 0))
      |> Enum.shuffle()
      # [{0, id}, {1, id}, ...]
      |> Enum.map_reduce(0, fn p, acc -> {{acc, p}, acc + 1} end)
      |> elem(0)
      |> Enum.group_by(fn {i, _} -> Integer.to_string(div(i, group_size)) end, fn {_, p} -> p end)

    updater = fn player, group_id, role ->
      player
      |> Map.merge(%{
        role: role,
        group_id: group_id,
        point: 0
      })
    end

    reducer = fn {group_id, ids}, {players, groups} ->
      if length(ids) == 2 do
        [id1, id2] = ids

        players =
          players
          |> Map.update!(id1, &updater.(&1, group_id, "proposer"))
          |> Map.update!(id2, &updater.(&1, group_id, "responder"))
          |> put_in([id1, :status], "allocating")
          |> put_in([id2, :status], "waiting")
          |> put_in([id1, :round], 1)
          |> put_in([id2, :round], 1)

        groups = groups |> Map.put(group_id, new_group(ids, initial_amount))
        {players, groups}
      else
        [id1] = ids

        players =
          players
          |> Map.update!(id1, &updater.(&1, group_id, "visitor"))
          |> put_in([id1, :status], "visiting")

        groups =
          groups
          |> Map.put(group_id, new_group(ids, initial_amount))
          |> put_in([group_id, :game_state], "finished")

        {players, groups}
      end
    end

    {players, groups} = Enum.reduce(groups, {players, %{}}, reducer)

    state
    |> Map.merge(%{
      players: players,
      groups: groups
    })
  end

  def new_group(members, initial_amount) do
    %{
      members: members,
      now_round: 1,
      redo_count: 0,
      allo_temp: div(initial_amount, 2),
      game_state: "allocating",
      group_results: [],
      response: ""
    }
  end

  defp reset(state) do
    # matchedを変更
    # playersとgroupsを変更
    new_state = match_groups(state, 2)

    new_state
    |> Map.merge(%{
      matched: true
    })
  end

  defp update_locales(state, locales_temp) do
    # localeを変更
    Enum.reduce(Map.keys(locales_temp), state, fn key, acc_state ->
      lang_temp = locales_temp[key]

      update_in(
        acc_state,
        [:locales, String.to_existing_atom(key), :translations],
        fn translations ->
          Enum.reduce(Map.keys(lang_temp), translations, fn trans_key, acc_trans ->
            put_in(
              acc_trans,
              [:variables, String.to_existing_atom(trans_key)],
              lang_temp[trans_key]
            )
          end)
        end
      )
    end)
  end

  defp get_next_role(role) do
    case role == "responder" do
      true -> "proposer"
      false -> "responder"
    end
  end

  defp new_player(_state) do
    %{
      # page: state.page,
      role: nil,
      group_id: nil,
      point: 0,
      read: false,
      status: "waiting",
      round: 1
    }
  end

  defp response(id, request_id, status \\ :ok, payload \\ %{}) do
    send(elem(request_id, 0), {:response, request_id, payload})
    :ok
  end
end
