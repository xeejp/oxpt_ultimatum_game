defmodule Oxpt.UltimatumGame.Guest.PlayerSocket do
  defstruct [:game_id, :guest_id, :channel_pid]

  use Oxpt.PlayerSocket

  alias Cizen.{Dispatcher, Filter, Event}
  alias Oxpt.Player.{Input, Output, Request}

  alias Oxpt.UltimatumGame.Events.{
    UpdateState,
    UpdateStateAll,
    UpdateStateGroup,
    FetchState,
    Read,
    ChangeAlloTemp,
    FinishAllocating,
    Response,
    ResetResponse,
    RedoAllocating
  }

  require Filter

  @impl GenServer
  def init(%__MODULE__{game_id: game_id, guest_id: guest_id} = socket) do
    Dispatcher.listen(
      Filter.any([
        Filter.new(fn %Event{body: %UpdateStateAll{game_id: ^game_id}} -> true end),
        Filter.new(fn %Event{body: %UpdateState{game_id: ^game_id, guest_id: ^guest_id}} ->
          true
        end),
        Filter.new(fn %Event{
                        body: %UpdateStateGroup{
                          game_id: ^game_id,
                          group_member_id: group_member_id
                        }
                      } ->
          Enum.member?(group_member_id, guest_id)
        end)
      ])
    )

    Dispatcher.dispatch(
      Event.new(
        nil,
        %FetchState{
          game_id: game_id,
          guest_id: guest_id
        }
      )
    )

    state = %{
      throttled?: false,
      updated?: false,
      state: %{}
    }

    {:ok, socket |> Map.from_struct() |> Map.merge(state)}
  end

  @impl GenServer
  def handle_info(%Event{body: %event_type{event: event, state: next_state}}, state)
      when event_type in [UpdateStateAll, UpdateState, UpdateStateGroup] do
    state = update_in(state.state, &Map.merge(&1, get_merge_data(next_state, state.guest_id)))
    state = put_in(state.updated?, true)
    {:noreply, state, {:continue, :try_send}} 
  end

  @impl GenServer
  def handle_info(:end_of_throttled_interval, state) do
    state = put_in(state.throttled?, false)

    {:noreply, state, {:continue, :try_send}}
  end

  @impl GenServer
  def handle_info({:response, _, _}, state), do: {:noreply, state}

  @impl GenServer
  def handle_continue(:try_send, state) do
    state =
      if state.updated? and not state.throttled? do
        send(
          state.channel_pid,
          %Output{
            event: "update_state",
            payload: %{state: state.state}
          }
        )

        state = put_in(state.throttled?, true)
        state = put_in(state.updated?, false)
        # 30fps
        Process.send_after(self(), :end_of_throttled_interval, 33)
        state
      else
        state
      end

    {:noreply, state}
  end

  @impl GenServer
  def handle_cast(%Input{event: "read"}, state) do
    Dispatcher.dispatch(
      Event.new(nil, %Read{
        game_id: state.game_id,
        guest_id: state.guest_id
      })
    )

    {:noreply, state}
  end

  def handle_call(
        %Request{event: "change allo temp", payload: allo_temp, timeout: timeout},
        from,
        state
      ) do
    request_id = {self(), from}

    Dispatcher.dispatch(
      Event.new(nil, %ChangeAlloTemp{
        game_id: state.game_id,
        guest_id: state.guest_id,
        request_id: request_id,
        allo_temp: allo_temp
      })
    )

    receive do
      {:response, ^request_id, payload} ->
        {:reply, payload, state}
    after
      timeout ->
        {:noreply, state}
    end
  end

  def handle_call(
        %Request{event: "finish allocating", payload: value, timeout: timeout},
        from,
        state
      ) do
    request_id = {self(), from}

    Dispatcher.dispatch(
      Event.new(nil, %FinishAllocating{
        game_id: state.game_id,
        guest_id: state.guest_id,
        request_id: request_id,
        value: value
      })
    )

    receive do
      {:response, ^request_id, payload} ->
        {:reply, payload, state}
    after
      timeout ->
        {:noreply, state}
    end
  end

  def handle_call(%Request{event: "response", payload: accept, timeout: timeout}, from, state) do
    request_id = {self(), from}

    Dispatcher.dispatch(
      Event.new(nil, %Response{
        game_id: state.game_id,
        guest_id: state.guest_id,
        request_id: request_id,
        accept: accept
      })
    )

    receive do
      {:response, ^request_id, payload} ->
        {:reply, payload, state}
    after
      timeout ->
        {:noreply, state}
    end
  end

  def handle_call(%Request{event: "reset response", timeout: timeout}, from, state) do
    request_id = {self(), from}

    Dispatcher.dispatch(
      Event.new(nil, %ResetResponse{
        game_id: state.game_id,
        guest_id: state.guest_id,
        request_id: request_id
      })
    )

    receive do
      {:response, ^request_id, payload} ->
        {:reply, payload, state}
    after
      timeout ->
        {:noreply, state}
    end
  end

  def handle_call(%Request{event: "redo allocating", timeout: timeout}, from, state) do
    request_id = {self(), from}

    Dispatcher.dispatch(
      Event.new(nil, %RedoAllocating{
        game_id: state.game_id,
        guest_id: state.guest_id,
        request_id: request_id
      })
    )

    receive do
      {:response, ^request_id, payload} ->
        {:reply, payload, state}
    after
      timeout ->
        {:noreply, state}
    end
  end

  defp get_merge_data(state, guest_id) do
    player = get_in(state, [:players, guest_id]) || %{}
    group = get_in(state, [:groups, get_in(player, [:group_id])])

    state
    |> Map.drop([:players, :groups])
    |> Map.merge(if group, do: %{group: group}, else: %{})
    |> Map.merge(player)
  end
end
