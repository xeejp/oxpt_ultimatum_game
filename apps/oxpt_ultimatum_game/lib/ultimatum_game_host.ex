defmodule Oxpt.UltimatumGame.Host do
  @moduledoc """
  Documentation for Oxpt.UltimatumGame.Host
  """

  use Cizen.Automaton
  defstruct [:room_id, :guest_game_id]

  use Cizen.Effects
  use Cizen.Effectful
  alias Oxpt.Persistence

  alias Cizen.{Event, Filter}
  alias Oxpt.Player.{Input, Request}
  alias Oxpt.UltimatumGame.Events.{UpdateStateAll, UpdateState, UpdateStateGroup}

  alias Oxpt.GetLog

  use Oxpt.Game,
    root_dir: Path.join(__ENV__.file, "../../host") |> Path.expand()

  @impl Oxpt.Game
  def player_socket(game_id, %__MODULE__{guest_game_id: guest_game_id}, guest_id) do
    %__MODULE__.PlayerSocket{
      game_id: game_id,
      guest_id: guest_id,
      guest_game_id: guest_game_id
    }
  end

  @impl Oxpt.Game
  def new(room_id, params) do
    %__MODULE__{room_id: room_id, guest_game_id: params[:game_id]}
  end

  @impl true
  def spawn(id, %__MODULE__{room_id: room_id, guest_game_id: guest_game_id}) do
    Persistence.Game.setup(id, room_id)

    perform(id, %Subscribe{
      event_filter:
        Filter.any([
          Filter.new(fn %Event{body: %Input{game_id: game_id}} ->
            game_id == id or game_id == guest_game_id
          end),
          Filter.new(fn %Event{body: %UpdateStateAll{game_id: ^guest_game_id}} ->
            true
          end),
          Filter.new(fn %Event{body: %UpdateState{game_id: ^guest_game_id}} ->
            true
          end),
          Filter.new(fn %Event{body: %UpdateStateGroup{game_id: ^guest_game_id}} ->
            true
          end),
          Filter.new(fn %Event{body: %Request{game_id: game_id}} ->
            game_id == id or game_id == guest_game_id
          end),
          Filter.new(fn %Event{body: %GetLog{game_id: ^id}} -> true end)
        ])
    })

    initial_state = %{
      log: []
    }

    {:loop, initial_state}
  end

  @impl true
  def yield(id, {:loop, state}) do
    event = perform(id, %Receive{})

    state =
      case event.body do
        %GetLog{} ->
          perform(id, %Dispatch{
            body: %GetLog.Response{
              request_id: event.id,
              log: ["Timestamp\tEvent"] ++ state.log
            }
          })

          state

        event_body ->
          update_in(state, [:log], fn log ->
            [
              "#{Date.utc_today()}_#{Time.utc_now()}\t#{inspect(event_body)}"
            ] ++ log
          end)
      end

    {:loop, state}
  end
end
