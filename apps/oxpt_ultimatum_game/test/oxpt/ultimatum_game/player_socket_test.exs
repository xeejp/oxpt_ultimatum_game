defmodule Oxpt.UltimatumGame.Guest.PlayerSocket.Test do
    use ExUnit.Case
  
    alias Cizen.{Dispatcher, Event, Filter, SagaID}
    alias Oxpt.Player.Output
    alias Oxpt.UltimatumGame.Guest.PlayerSocket
  
    alias Oxpt.UltimatumGame.Events.{
      UpdateState,
      UpdateStateAll
    }
  
    require Filter
  
    setup do
      game_id = SagaID.new()
      guest_id = SagaID.new()
  
      {:ok, _} =
        GenServer.start_link(PlayerSocket, %PlayerSocket{
          game_id: game_id,
          guest_id: guest_id,
          channel_pid: self()
        })
  
      {:ok, game_id: game_id}
    end
  
    test "dispatches Output after UpdateStateAll", %{game_id: game_id} do
      Dispatcher.dispatch(
        Event.new(nil, %UpdateStateAll{
          game_id: game_id,
          state: %{a: 1}
        })
      )
  
      assert_receive %Output{event: "update_state", payload: %{state: %{a: 1}}}
    end
  
    test "dispatches Output after UpdateStateAll with interval", %{game_id: game_id} do
      Dispatcher.dispatch(
        Event.new(nil, %UpdateStateAll{
          game_id: game_id,
          state: %{a: 1}
        })
      )
      Dispatcher.dispatch(
        Event.new(nil, %UpdateStateAll{
          game_id: game_id,
          state: %{b: 2}
        })
      )
      :timer.sleep(100)
      Dispatcher.dispatch(
        Event.new(nil, %UpdateStateAll{
          game_id: game_id,
          state: %{c: 3}
        })
      )
  
      assert_receive %Output{event: "update_state", payload: %{state: %{a: 1}}}
      assert_receive %Output{event: "update_state", payload: %{state: %{a: 1, b: 2}}}
      assert_receive %Output{event: "update_state", payload: %{state: %{a: 1, b: 2, c: 3}}}
    end
  
    test "dispatches one Output after UpdateStateAll with small interval", %{game_id: game_id} do
      Dispatcher.dispatch(
        Event.new(nil, %UpdateStateAll{
          game_id: game_id,
          state: %{a: 1}
        })
      )
      Dispatcher.dispatch(
        Event.new(nil, %UpdateStateAll{
          game_id: game_id,
          state: %{b: 2}
        })
      )
      Dispatcher.dispatch(
        Event.new(nil, %UpdateStateAll{
          game_id: game_id,
          state: %{c: 3}
        })
      )
  
      assert_receive %Output{event: "update_state", payload: %{state: %{a: 1}}}
      assert_receive %Output{event: "update_state", payload: %{state: %{a: 1, b: 2, c: 3}}}
      refute_receive %Output{event: "update_state", payload: %{state: %{a: 1, b: 2}}}
    end
  end
  
